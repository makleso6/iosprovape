//
//  SCMainNavigationController.h
//  Divoxi
//
//  Created by Zolotarev Alexander on 12.02.16.
//  Copyright © 2016 Sugar and Candy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SCMainNavigationController : UINavigationController

@end
