//
//  SCBaseModalViewController.h
//  Divoxi
//
//  Created by Zolotarev Alexander on 12.02.16.
//  Copyright © 2016 Sugar and Candy. All rights reserved.
//

#import "SCBaseViewController.h"

@interface SCBaseModalViewController : SCBaseViewController

- (void)setupCloseModal;
- (void)dismissViewControllerWithAnimation;

@end
