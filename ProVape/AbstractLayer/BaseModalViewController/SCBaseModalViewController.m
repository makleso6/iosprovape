//
//  SCBaseModalViewController.m
//  Divoxi
//
//  Created by Zolotarev Alexander on 12.02.16.
//  Copyright © 2016 Sugar and Candy. All rights reserved.
//

#import "SCBaseModalViewController.h"
#import "UIBarButtonItem+ButtonApearance.h"

@interface SCBaseModalViewController ()

@end

@implementation SCBaseModalViewController

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupCloseModal];
}

#pragma mark - Other

- (void)setupCloseModal {
    if (self.navigationController) {
        self.navigationItem.rightBarButtonItem = [UIBarButtonItem barButtonItemCloseModalWithTarget:self
                                                                                         withAction:@selector(dismissViewControllerWithAnimation)];
    }
}

- (void)dismissViewControllerWithAnimation {
    [self.view endEditing:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
