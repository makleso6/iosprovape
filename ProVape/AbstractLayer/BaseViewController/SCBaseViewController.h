//
//  SCBaseViewController.h
//  Divoxi
//
//  Created by Zolotarev Alexander on 12.02.16.
//  Copyright © 2016 Sugar and Candy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SCBaseViewController : UIViewController

///Включить/Выключить скрытие клавиатуры по self.view. Изначально NO.
@property (assign, nonatomic, getter=isEnabledDismissKeyboardOnTap) BOOL enabledDismissKeyboardOnTap;

- (void)setupInteractivePop;
- (void)setupBackButton;

@end
