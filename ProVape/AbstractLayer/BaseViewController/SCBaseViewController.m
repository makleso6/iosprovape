//
//  SCBaseViewController.m
//  Divoxi
//
//  Created by Zolotarev Alexander on 12.02.16.
//  Copyright © 2016 Sugar and Candy. All rights reserved.
//

#import "SCBaseViewController.h"
#import "UIBarButtonItem+ButtonApearance.h"

@interface SCBaseViewController () <UIGestureRecognizerDelegate>

@property (strong, nonatomic) UITapGestureRecognizer *closeKeyboardGesture;

@end

@implementation SCBaseViewController

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    UITapGestureRecognizer *closeKeyboardGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                           action:@selector(dismissKeyboardOnTapAction:)];
    self.closeKeyboardGesture = closeKeyboardGesture;
    if (self.isEnabledDismissKeyboardOnTap) {
        [self.view addGestureRecognizer:closeKeyboardGesture];
    }
    [self setupInteractivePop];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setupBackButton];
}

- (void)setEnabledDismissKeyboardOnTap:(BOOL)enabledDismissKeyboardOnTap {
    _enabledDismissKeyboardOnTap = enabledDismissKeyboardOnTap;
    if (!enabledDismissKeyboardOnTap) {
        [self.view removeGestureRecognizer:self.closeKeyboardGesture];
    } else {
        [self.view addGestureRecognizer:self.closeKeyboardGesture];
    }
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    if ([gestureRecognizer isKindOfClass: [UIScreenEdgePanGestureRecognizer class]] && self.navigationController.viewControllers.count<=1) {
        return NO;
    }
    return YES;
}

#pragma mark - Actions

- (void)dismissKeyboardOnTapAction:(UITapGestureRecognizer *)sender {
    if (self.isEnabledDismissKeyboardOnTap) {
        [self.view endEditing:YES];
    }
}

#pragma mark - Other

- (void)setupInteractivePop {
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
}

- (void)setupBackButton {
    if (self.navigationController.viewControllers.count > 1) {
        self.navigationItem.leftBarButtonItems = [UIBarButtonItem barButtonBackArrowWithTarget:self.navigationController
                                                                                    withAction:@selector(popViewControllerAnimated:)];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
