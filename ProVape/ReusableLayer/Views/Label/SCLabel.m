//
//  SCLabel.m
//  ProVape
//
//  Created by Maxim Kolesnik on 01.11.15.
//  Copyright © 2015 Sugar And Candy LLC. All rights reserved.
//

#import "SCLabel.h"

@implementation SCLabel

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self applyAttributes];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self applyAttributes];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self applyAttributes];
    }
    return self;
}

-(void)applyCommonAttributes {
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    UIColor *color = [UIColor whiteColor];
    
    [dict setObject:color forKey:NSForegroundColorAttributeName];
    
    self.labelAttributes = [NSDictionary dictionaryWithDictionary:dict];
}

-(void)applyAttributes {
    
    [self applyCommonAttributes];

}

@end
