//
//  SCPlaceholderTextField.m
//  ProVape
//
//  Created by Maxim Kolesnik on 31.10.15.
//  Copyright © 2015 Sugar And Candy LLC. All rights reserved.
//

#import "SCPlaceholderTextField.h"

@implementation SCPlaceholderTextField

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self applyAttributes];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self applyAttributes];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self applyAttributes];
    }
    return self;
}

-(void)applyCommonAttributes {
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    UIColor *color = [UIColor whiteColor];

    [dict setObject:color forKey:NSForegroundColorAttributeName];
    
    self.placeholderAttributes = [NSDictionary dictionaryWithDictionary:dict];
}

-(void)applyAttributes {
    
    [self applyCommonAttributes];
    
    if ([self respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        self.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.placeholder attributes:self.placeholderAttributes];
    }
}

@end
