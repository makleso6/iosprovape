//
//  SCPlaceholderTextField.h
//  ProVape
//
//  Created by Maxim Kolesnik on 31.10.15.
//  Copyright © 2015 Sugar And Candy LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SCPlaceholderTextField : UITextField

@property (nonatomic, strong) NSDictionary *placeholderAttributes;

-(void)applyAttributes;

@end
