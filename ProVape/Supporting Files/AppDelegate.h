//
//  AppDelegate.h
//  ProVape
//
//  Created by Maxim Kolesnik on 29.10.15.
//  Copyright © 2015 Sugar And Candy LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

