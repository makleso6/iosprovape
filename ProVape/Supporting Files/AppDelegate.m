//
//  AppDelegate.m
//  ProVape
//
//  Created by Maxim Kolesnik on 29.10.15.
//  Copyright © 2015 Sugar And Candy LLC. All rights reserved.
//

#import "AppDelegate.h"


static NSString *names[] = {@"max",@"alex",@"vova",@"dima",@"sasha",@"vitya"};
static NSString *passwords[] = {@"maasdfx",@"alsex",@"vowretsfdgva",@"dimqrwea",@"sasadsfha",@"vi13425tya"};


@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [MagicalRecord setupCoreDataStackWithStoreNamed:@"ProVape"];
    
    [Parse enableLocalDatastore];
    
    [Parse setApplicationId:@"FxrAiHo4g9G3nFPabXvKDeaVUPPAAbt3zkJkzyvM"
                  clientKey:@"KgK980KiFljiVo5Y93EmYgONQY06KEpcvearyeSM"];
    
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert |
                                                        UIUserNotificationTypeBadge |
                                                        UIUserNotificationTypeSound);
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes
                                                                                 categories:nil];
        [application registerUserNotificationSettings:settings];
        [application registerForRemoteNotifications];
    }
    
    KVNProgressConfiguration *config = [[KVNProgressConfiguration alloc]init];
    
    config.backgroundTintColor = [UIColor colorWithRed:32.0f/255.0f green:32.0f/255.0f blue:32.0f/255.0f alpha:1];
    
    config.statusColor = [UIColor whiteColor];
    
    config.circleStrokeForegroundColor = [UIColor whiteColor];
    
    config.errorColor = [UIColor whiteColor];
    
    config.successColor = [UIColor whiteColor];
    
    [KVNProgress setConfiguration:config];
    
    [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys: [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0], NSForegroundColorAttributeName, [UIFont fontWithName:@"Arial" size:0.0], NSFontAttributeName, nil]];
    
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    
    [[UINavigationBar appearance] setBarTintColor: [UIColor pv_navigationBarColor]];
    
    [[UINavigationBar appearance] setBarTintColor: [UIColor pv_lightBlackColor]];
    
    
    /*
     for (NSUInteger index = 0; index <20 ; index++) {
     PFObject *testObject = [PFObject objectWithClassName:@"User"];
     
     NSUInteger index = arc4random_uniform(6);
     
     //testObject[@"username"] = @"adfs";
     
     testObject[@"username"] = names[index];
     testObject[@"password"] = passwords[index];
     
     [testObject saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
     if (succeeded) {
     
     NSLog(@"SUCCESSED");
     
     }
     else {
     NSLog(@"%@",error.localizedDescription);
     }
     }];
     }
     */
    //[PFUser logOut];
    
    UIStoryboard *board;
    
    if ([PFUser currentUser]) {
        NSLog(@"AUTH");
        
        board = [UIStoryboard pv_getProVapeStoryboard];
    } else {
        NSLog(@"NO AUTH");
        board = [UIStoryboard pv_getMainStoryboard];
        
    }
    
    UIViewController *rootViewController = [board instantiateInitialViewController];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    self.window.rootViewController = rootViewController;
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    
    /*
     
     PFUser* user = [PFUser user]; // not the current user!
     
     user.username = @"makleso";
     
     user.email = @"mail@sugarandcandy.ru";
     
     user.password = @"makleso6";
     
     [user setObject:@"newuser@email.com" forKey:@"emailAddress"];
     [user setObject:@"123456" forKey:@"invitationKey"];
     
     [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
     if(error) {
     NSLog(@"Error saving user: %@", error);
     } else {
     NSLog(@"User %@ saved! You are logged in as %@", user, [PFUser currentUser]);
     
     NSLog(@"%@",[PFUser currentUser].sessionToken);
     }
     }];
     */
    
    
    //    [PFUser logInWithUsernameInBackground:@"makleso6" password:@"makleso6" block:^(PFUser * _Nullable user, NSError * _Nullable error) {
    //
    //        if (user) {
    //            NSLog(@"GHJKLHKL");
    //
    //            PFQuery *query = [PFQuery queryWithClassName:@"User"];
    //            [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
    //                if (!error) {
    //
    //                    NSLog(@"%@",objects);
    //                    // The find succeeded. The first 100 objects are available in objects
    //                } else {
    //                    // Log details of the failure
    //                    NSLog(@"Error: %@ %@", error, [error userInfo]);
    //                }
    //            }];
    //        }
    //
    //    }];
    
    
    //[self localStore];
    
    //[self queryFromLocal];
    
    //[self synhrolize];
    
    //[self testSubclass];
    
    //[self testCloudFunction];
    
    //[self testResetPassword];
    //    [self deleteAll];
    //    [self createData];
    //    [self createData];
    //    [self createData];
    //    [self createData];
    //
    
    [self generatePARSElocalData];
    
    //[[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError *error) {
    //[[SCSynchronizationManager standartManager] synchronizeWithServer:nil];
    //[[SCSynchronizationManager standartManager] synchronizeWithLocalStorage:nil];
    
    //  }];
    
    //
    //    }];
    
    
    
    return YES;
}

-(void)deleteAll {
    //[User MR_truncateAll];
    [Coil MR_truncateAll];
    [Rating MR_truncateAll];
}



-(void)generatePARSElocalData {
    
    NSMutableArray *coils = [NSMutableArray array];
    
    for (NSUInteger index = 0; index < 1; index ++) {
        
        PFCoil *coil = [PFCoil object];
        
        coil.resistance = @(((float)arc4random_uniform(1200)/10));
        
        coil.user = [PFUser currentUser];
        
        [coils addObject:coil];

  
    }
    
    for (NSUInteger index = 0; index < 4; index ++) {
        
        PFCoil *coil = [PFCoil object];
        
        coil.resistance = @(((float)arc4random_uniform(1200)/10));
        
        [coils addObject:coil];
        
        //coil.user = [PFUser currentUser];
        
    }
    
    [PFObject saveAllInBackground:coils block:^(BOOL succeeded, NSError * _Nullable error) {
        if (succeeded) {
            [self saveAllObjectsToLocal];
        }
    }];
    
    
    
}

-(void)saveAllObjectsToLocal {
    PFQuery *query = [PFQuery queryWithClassName:[PFCoil parseClassName]];
    
    query.limit = 1000;
//    [query fromLocalDatastore];
//    
//    [query findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
//        NSLog(@"%lu", [objects count]);
//        
//        
//    }];
    
    
    [query findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
        NSLog(@"%lu", [objects count]);
        
        [PFObject pinAllInBackground:objects block:^(BOOL succeeded, NSError * _Nullable error) {
            
            [query fromLocalDatastore];
            
            [query findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
                NSLog(@"%lu", [objects count]);
                
                
            }];
            
        }];
        
    }];
}

-(void)localStore{
    
    for (NSUInteger index = 0; index <100; index++) {
        PFObject *gameScore = [PFObject objectWithClassName:@"GameScore"];
        
        NSUInteger index = arc4random_uniform(6);
        
        gameScore[@"score"] = @(index * arc4random_uniform(100));
        
        
        
        gameScore[@"playerName"] = names[index];
        gameScore[@"cheatMode"] = @YES;
        [gameScore pinInBackground];
    }
    
    
    
    
    
}

-(void)queryFromLocal {
    PFQuery *query = [PFQuery queryWithClassName:@"GameScore"];
    [query fromLocalDatastore];
    //    [[query getObjectInBackgroundWithId:@"xWMyZ4YE"] continueWithBlock:^id(BFTask *task) {
    //        if (task.error) {
    //            // Something went wrong.
    //            return task;
    //        }
    //
    //        // task.result will be your game score
    //        return task;
    //    }];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
        NSLog(@"%@", objects);
        
        NSLog(@"%@", error);
    }];
    
    
}

-(void)synhrolize {
    
    PFQuery *query = [PFQuery queryWithClassName:@"GameScore"];
    //[query fromPinWithName:@"MyChanges"];
    
    [query fromLocalDatastore];
    
    
    [[query findObjectsInBackground] continueWithBlock:^id(BFTask *task) {
        NSArray *scores = task.result;
        
        NSLog(@"%@", task.result);
        
        for (PFObject *score in scores) {
            [[score saveInBackground] continueWithSuccessBlock:^id(BFTask *task) {
                return [score unpinInBackground];
                
            }];
        }
        return task;
    }];
    
}

-(void)testSubclass {
    
    //for (NSUInteger index = 0; index < 1000; index ++) {
    PFCoil *someCoil = [PFCoil object];
    
    someCoil.wireDiameter = @(2);
    
    
    [someCoil setObject:[PFUser currentUser] forKey:@"user"];
    
    
    [someCoil saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
        if (succeeded) {
            
            NSLog(@"SUCCESSED");
            
        }
        else {
            NSLog(@"%@",error.localizedDescription);
        }
    }];
    //}
    
    
    
}

-(void)testCloudFunction {
    [PFCloud callFunctionInBackground:@"hello"
                       withParameters:@{}
                                block:^(NSString *result, NSError *error) {
                                    
                                    NSLog(@"%@",result);
                                    if (!error) {
                                        // result is @"Hello world!"
                                    }
                                }];
}

-(void)testResetPassword {
    //[PFUser requestPasswordResetForEmailInBackground:@"makleso6@mail.ru"];
    
    [PFUser requestPasswordResetForEmailInBackground:@"makleso6@mail.ru" block:^(BOOL succeeded, NSError * _Nullable error) {
        if (succeeded) {
            NSLog(@"УСПЕХ");
        }
        
        else NSLog(@"%@",error.localizedDescription);
    }];
    
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
}

#pragma mark Push Notifications

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:deviceToken];
    [currentInstallation saveInBackground];
    
    [PFPush subscribeToChannelInBackground:@"" block:^(BOOL succeeded, NSError *error) {
        if (succeeded) {
            NSLog(@"ParseStarterProject successfully subscribed to push notifications on the broadcast channel.");
        } else {
            NSLog(@"ParseStarterProject failed to subscribe to push notifications on the broadcast channel.");
        }
    }];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    if (error.code == 3010) {
        NSLog(@"Push notifications are not supported in the iOS Simulator.");
    } else {
        // show some alert or otherwise handle the failure to register.
        NSLog(@"application:didFailToRegisterForRemoteNotificationsWithError: %@", error);
    }
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    //[PFPush handlePush:userInfo];
    
    if (application.applicationState == UIApplicationStateInactive) {
        [PFAnalytics trackAppOpenedWithRemoteNotificationPayload:userInfo];
    }
}

@end
