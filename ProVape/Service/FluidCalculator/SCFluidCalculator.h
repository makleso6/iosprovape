//
//  SCFluidCalculator.h
//  Coil Pro
//
//  Created by Максим Колесник on 26.09.15.
//  Copyright © 2015 Sugar And Candy LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface SCFluidCalculator : NSObject

/**
 *  Крепость в базовой жидкости
 */
@property (assign, nonatomic) CGFloat fortressInBaseFluid;

/**
 *  Желаемая крепость на выходе
 */
@property (assign, nonatomic) CGFloat desiredFortressOutput;

/**
 *  Процент пропиленгликоля (PG)
 */
@property (assign, nonatomic) CGFloat percentagePG;

/**
 *  Процент пищевого глицерина (VG)
 */
@property (assign, nonatomic) CGFloat percentageVG;

/**
 *  Процент дистиллированной воды
 */
@property (assign, nonatomic) CGFloat percentageWater;

/**
 *  Процент ароматизатора
 */
@property (assign, nonatomic) CGFloat percentageFlavour;

/**
 *  Требуемый объём жидкости на выходе
 */
@property (assign, nonatomic) CGFloat requiredVolume;

/**
 *  Количество капель в ml
 */
@property (assign, nonatomic) CGFloat numberOfDropsPerML;

#pragma mark - Result

/**
 *  Колличество ароматизатора
 */
@property (nonatomic, assign, readonly) CGFloat flavours;

/**
 *  Колличество воды
 */
@property (nonatomic, assign, readonly) CGFloat water;

/**
 *  Колличество приполенгликоля
 */
@property (nonatomic, assign, readonly) CGFloat propyleneGlycol;

/**
 *  Колличество глицерина
 */
@property (nonatomic, assign, readonly) CGFloat glycerin;

/**
 *  Колличество основы с никотином
 */
@property (nonatomic, assign, readonly) CGFloat baseFluid;

@end
