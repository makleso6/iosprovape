//
//  SCFluidCalculator.m
//  Coil Pro
//
//  Created by Максим Колесник on 26.09.15.
//  Copyright © 2015 Sugar And Candy LLC. All rights reserved.
//

#import "SCFluidCalculator.h"

@interface SCFluidCalculator ()

/**
 *  Колличество ароматизатора
 */
@property (nonatomic, assign) CGFloat flavours;

/**
 *  Колличество воды
 */
@property (nonatomic, assign,) CGFloat water;

/**
 *  Колличество приполенгликоля
 */
@property (nonatomic, assign) CGFloat propyleneGlycol;

/**
 *  Колличество глицерина
 */
@property (nonatomic, assign) CGFloat glycerin;

/**
 *  Колличество основы с никотином
 */
@property (nonatomic, assign) CGFloat baseFluid;



@end

@implementation SCFluidCalculator

-(void)reCalculate {
    
}

-(CGFloat)baseFluid{
    
    _baseFluid = (self.requiredVolume/(self.fortressInBaseFluid/self.desiredFortressOutput));

    return _baseFluid;
}

-(CGFloat)propyleneGlycol{
    
    _propyleneGlycol = ((self.requiredVolume - self.baseFluid - self.flavours) * (self.percentagePG/100));
    
    return _propyleneGlycol;
}

-(CGFloat)glycerin{

    _glycerin = ((self.requiredVolume - self.baseFluid - self.flavours) * (self.percentageVG/100));
    
    return _glycerin;

}

-(CGFloat)water{
    
    _water = ((self.requiredVolume - self.baseFluid - self.flavours) * (self.percentageWater/100));
    return _water;
}

-(CGFloat)flavours{
    
    _flavours = self.requiredVolume * (self.percentageFlavour/100);
    
    return _flavours;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        

        self.fortressInBaseFluid = 80;
        self.desiredFortressOutput = 3;
        self.percentageVG = 55;
        self.percentagePG = 45;
        self.percentageWater = 0;
        self.percentageFlavour = 0;
        self.requiredVolume = 100;
        self.numberOfDropsPerML = 33;
        
        NSLog(@"%f", [self baseFluid]);
        NSLog(@"%f", [self propyleneGlycol]);
        NSLog(@"%f", [self glycerin]);
        NSLog(@"%f", [self water]);
        NSLog(@"%f", [self flavours]);

    
        
    }
    return self;
}



-(BOOL)amountOfglycerin:(CGFloat)vg propyleneGlycol:(CGFloat)pg water:(CGFloat)water{
    
    NSLog(@"\nglycerin = %f\npropyleneGlycol = %f\nwater = %f", vg, pg, water);
    
    NSLog(@"amount%f", vg + pg + water);
    
    if ((vg+pg+water) == 100) {
        return YES;
    }
    
    CGFloat persentVG = 0, persentPG = 0, persentWater = 0;
    
    CGFloat amount = vg + pg + water;
    
    persentVG = vg/amount;
    
    persentPG = pg/amount;
    
    persentWater = water/amount;
    
    NSLog(@"\nglycerin = %f\npropyleneGlycol = %f\nwater = %f\n%f", persentVG, persentPG, persentWater,persentVG+persentPG+persentWater);


    NSLog(@"pg = %f",persentPG * 100);
    NSLog(@"vg = %f",persentVG * 100);
    NSLog(@"water = %f",persentWater * 100);

    NSLog(@"amount = %f",(persentPG * 100)+(persentVG * 100)+(persentWater * 100));
    

    
    return NO;
    
}



@end
