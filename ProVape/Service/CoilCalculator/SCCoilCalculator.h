//
//  AZCalculatorManager.h
//  CoilCalc
//
//  Created by Sasha on 14.04.15.
//  Copyright (c) 2015 maxim kolesnik. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, SCCoilCalculatorCoilType) {
    SCCoilCalculatorCoilTypeMicro = 1,
    SCCoilCalculatorCoilTypeNormal = 2
};

SCCALCILATION_EXTERN NSString * const SCOrderTypeWiresInCoil;
SCCALCILATION_EXTERN NSString * const SCOrderTypeWireDiameter;
SCCALCILATION_EXTERN NSString * const SCOrderTypeWireType;
SCCALCILATION_EXTERN NSString * const SCOrderTypeWireLegsLength;

SCCALCILATION_EXTERN NSString * const SCOrderTypeCoilsNumber;
SCCALCILATION_EXTERN NSString * const SCOrderTypeCoilType;

SCCALCILATION_EXTERN NSString * const SCOrderTypeTurnsDiameter;
SCCALCILATION_EXTERN NSString * const SCOrderTypeTurnsNumber;

SCCALCILATION_EXTERN NSString * const SCOrderTypeOhmCorrection;
SCCALCILATION_EXTERN NSString * const SCOrderTypeIsTwisted;
SCCALCILATION_EXTERN NSString * const SCOrderTypeVoltage;


extern const struct SCCoilCalculatorWireType {
     CGFloat nickel200;
     CGFloat NiFe30;
     CGFloat dicodesResist;
     CGFloat NiFe48;
     CGFloat titanium;
     CGFloat SSAISI316;
     CGFloat SSAISI340;
     CGFloat nichromeNi80;
     CGFloat nichromeNi60;
     CGFloat fechral;
     CGFloat kanthalD;
     CGFloat kanthalA1;

} SCCoilCalculatorWireType;

/***** singleton manager for coil calculate *****/

@interface SCCoilCalculator : NSObject


@property (assign, nonatomic) BOOL ohmCorrection;
@property (assign, nonatomic) BOOL twisted;


/**
 *  @brief  WIRE_OPTIONS
 */

@property (assign, nonatomic) NSInteger wiresInCoil;
@property (assign, nonatomic) CGFloat wireDiameter;
@property (assign, nonatomic) CGFloat wireType;
@property (assign, nonatomic) CGFloat wireLengsLength;

/**
 *  @brief  COIL_OPTIONS
 */

@property (assign, nonatomic) NSInteger coilsNumber;
@property (assign, nonatomic) NSInteger coilType;

/**
 *  @brief  TURNS_OPTIONS
 */

@property (assign, nonatomic) CGFloat turnDiameter;
@property (assign, nonatomic) NSInteger turnsNumber;

/**
 *  @brief  power options
 */

@property (assign, nonatomic) CGFloat voltage;

/**
 *  @brief  output
 */
@property (assign, nonatomic, readonly) CGFloat currentWatts;
@property (assign, nonatomic, readonly) CGFloat powerRecomender;
@property (assign, nonatomic, readonly) CGFloat resistance;

@property (assign, nonatomic, readonly) CGFloat wireLenght;
@property (assign, nonatomic, readonly) CGFloat current;
@property (assign, nonatomic, readonly) CGFloat powerDensity;
@property (assign, nonatomic, readonly) CGFloat coilLenght;
@property (assign, nonatomic, readonly) CGFloat tempDry;

/********* methods *********/
/*
 Argument "settings" type NSArray* must include all properties (without ressitance)
 arranged in the order (REQUIRED)!!!:"wiresInCoil, coilsNumber,..., voltage".
 First of all, the method check length array. If the length is not equal to 11 - the method returns nill.
 If the order is not valid - you may receive an invalid instance.
 Please checks you order.
 */
+ (SCCoilCalculator*)sharedInstanceWithStandartSettings:(NSArray *)settings;

/* REQUIRED SETTINGS */
+ (NSArray *) getRequiredSettings;

/* default call instance */
+ (SCCoilCalculator*) sharedInstance;

@end
