//
//  AZCalculatorManager.m
//  CoilCalc
//
//  Created by Sasha on 14.04.15.
//  Copyright (c) 2015 maxim kolesnik. All rights reserved.
//

#import "SCCoilCalculator.h"
#define OHM_CORRECT  1.1f;

const struct SCCoilCalculatorWireType SCCoilCalculatorWireType = {
    .nickel200         = 0.09f,
    .NiFe30            = 0.24f,
    .dicodesResist     = 0.28f,
    .NiFe48            = 0.36f,
    .titanium          = 0.42f,
    .SSAISI316         = 0.74,
    .SSAISI340         = 0.8f,
    .nichromeNi80      = 1.08f,
    .nichromeNi60      = 1.11,
    .fechral           = 1.39f,
    .kanthalD          = 1.35,
    .kanthalA1         = 1.45f,
    
};

typedef NS_ENUM(NSInteger, AZOrderType){
    
    AZOrderTypeWiresInCoil,
    AZOrderTypeCoilsNumber,
    AZOrderTypeWireDiameter,
    AZOrderTypeCoilDiameter,
    AZOrderTypeWindingsNumber,
    AZOrderTypeLegsLength,
    AZOrderTypeWireType,
    AZOrderTypeCoilType,
    AZOrderTypeOhmCorrection,
    AZOrderTypeIsTwisted,
    AZOrderTypeVoltage
    
};

@implementation SCCoilCalculator

#pragma mark - OVERRIDE METHODS

- (void) dealloc {
    
    NSLog(@"AZCalculatorManager deallocated");
    [self removeObserver:self forKeyPath:NSStringFromSelector(@selector(wiresInCoil))];
    [self removeObserver:self forKeyPath:NSStringFromSelector(@selector(coilsNumber))];
    [self removeObserver:self forKeyPath:NSStringFromSelector(@selector(wireDiameter))];
    [self removeObserver:self forKeyPath:NSStringFromSelector(@selector(turnDiameter))];
    [self removeObserver:self forKeyPath:NSStringFromSelector(@selector(turnsNumber))];
    [self removeObserver:self forKeyPath:NSStringFromSelector(@selector(wireLengsLength))];
    [self removeObserver:self forKeyPath:NSStringFromSelector(@selector(wireType))];
    [self removeObserver:self forKeyPath:NSStringFromSelector(@selector(coilType))];
    [self removeObserver:self forKeyPath:NSStringFromSelector(@selector(ohmCorrection))];
    [self removeObserver:self forKeyPath:NSStringFromSelector(@selector(twisted))];
    [self removeObserver:self forKeyPath:NSStringFromSelector(@selector(voltage))];

    
    
}

#pragma mark - Observing

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    
    [self reCalculate];
}

#pragma mark - PUBLIC METHODS

+ (SCCoilCalculator*) sharedInstanceWithStandartSettings: (NSArray *) settings {
    
    SCCoilCalculator *calculator = [SCCoilCalculator sharedInstance];

    if (settings.count == 11) {
        
        calculator.wiresInCoil     = [[settings objectAtIndex: AZOrderTypeWiresInCoil] floatValue];
        calculator.coilsNumber     = [[settings objectAtIndex: AZOrderTypeCoilsNumber] floatValue];
        calculator.wireDiameter    = [[settings objectAtIndex: AZOrderTypeWireDiameter] floatValue];
        calculator.turnDiameter    = [[settings objectAtIndex: AZOrderTypeCoilDiameter] floatValue];
        calculator.turnsNumber     = [[settings objectAtIndex: AZOrderTypeWindingsNumber] floatValue];
        calculator.wireLengsLength = [[settings objectAtIndex: AZOrderTypeLegsLength] floatValue];
        calculator.wireType        = [[settings objectAtIndex: AZOrderTypeWireType] floatValue];
        calculator.voltage         = [[settings objectAtIndex: AZOrderTypeVoltage] floatValue];
        calculator.coilType        = [[settings objectAtIndex: AZOrderTypeCoilType] integerValue];
        calculator.ohmCorrection   = [[settings objectAtIndex: AZOrderTypeOhmCorrection]boolValue];
        calculator.twisted         = [[settings objectAtIndex: AZOrderTypeVoltage]boolValue];
        
        [calculator addObserver: calculator
                     forKeyPath: NSStringFromSelector(@selector(wiresInCoil))
                        options: NSKeyValueObservingOptionNew
                        context: NULL];
        
        [calculator addObserver: calculator
                     forKeyPath: NSStringFromSelector(@selector(coilsNumber))
                        options: NSKeyValueObservingOptionNew
                        context: NULL];
        
        [calculator addObserver: calculator
                     forKeyPath: NSStringFromSelector(@selector(wireDiameter))
                        options: NSKeyValueObservingOptionNew
                        context: NULL];
        
        [calculator addObserver: calculator
                     forKeyPath: NSStringFromSelector(@selector(turnDiameter))
                        options: NSKeyValueObservingOptionNew
                        context: NULL];
        
        [calculator addObserver: calculator
                     forKeyPath: NSStringFromSelector(@selector(turnsNumber))
                        options: NSKeyValueObservingOptionNew
                        context: NULL];
        
        [calculator addObserver: calculator
                     forKeyPath: NSStringFromSelector(@selector(wireLengsLength))
                        options: NSKeyValueObservingOptionNew
                        context: NULL];
        
        [calculator addObserver: calculator
                     forKeyPath: NSStringFromSelector(@selector(wireType))
                        options: NSKeyValueObservingOptionNew
                        context: NULL];
        
        [calculator addObserver: calculator
                     forKeyPath: NSStringFromSelector(@selector(voltage))
                        options: NSKeyValueObservingOptionNew
                        context: NULL];
        
        [calculator addObserver: calculator
                     forKeyPath: NSStringFromSelector(@selector(coilType))
                        options: NSKeyValueObservingOptionNew
                        context: NULL];
        
        [calculator addObserver: calculator
                     forKeyPath: NSStringFromSelector(@selector(ohmCorrection))
                        options: NSKeyValueObservingOptionNew
                        context: NULL];
        
        [calculator addObserver: calculator
                     forKeyPath: NSStringFromSelector(@selector(twisted))
                        options: NSKeyValueObservingOptionNew
                        context: NULL];
        
        [calculator reCalculate];
        
    } else {
        
        return nil;
    }
    
    
    
    return calculator;
}

+ (NSArray *) getRequiredSettings {
    
    /**** expose the default settings ****/
    /*** for maxim ***/
    /** default settings must be is equal settings on UI  **/
    /* required settings:@[@1, @1, @0.32, @2, @7, @4, @1.47] last arg - Kantal A1  */
    
    static NSArray *requiredSettings;
    requiredSettings = @[@2, @2, @0.45, @3, @5, @2, @1.45,@1, @(NO), @(NO), @3.5];
    return requiredSettings;
}

+ (SCCoilCalculator*) sharedInstance {
    
    static SCCoilCalculator * sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[SCCoilCalculator alloc]init];
    });
    return sharedInstance;
}

#pragma mark - PRIVATE METHODS

/* calcilating method */
- (void) reCalculate {
    // Площадь сечения S mm2
    CGFloat r_area = M_PI * (pow((self.wireDiameter / 2), 2));
    
    //self.coilType = 1;
    
    // Длина провода спирали (одного) mm
    CGFloat av_diam = self.wireDiameter+self.turnDiameter;
    CGFloat r_wirelength = (sqrt(pow((M_PI*av_diam), 2) + pow(self.wireDiameter*self.wiresInCoil*self.coilType, 2) )) * self.turnsNumber + (self.wireLengsLength*2);
    if (self.twisted == 1 && self.wiresInCoil >= 2) {
        r_wirelength = r_wirelength * 1.2;
    }
    _wireLenght = r_wirelength;
    //if (self.wiresInCoil == 1) {  document.getElementById("twisted").checked=0; }
    
    
    // Сопротивление спирали R ом
    CGFloat r_resist = (self.wireType * (r_wirelength+(self.wiresInCoil*self.wireDiameter)) / r_area / 1000) / (self.coilsNumber * self.wiresInCoil);
    if (self.ohmCorrection == 1) {
        r_resist = r_resist * OHM_CORRECT;
    }
    

    
    _resistance = r_resist;
    
    
    // Мощность P ватт
    CGFloat r_power = pow((self.voltage), 2)/r_resist;
    _currentWatts = r_power;
    // Ток I ампер
    CGFloat r_curent = self.voltage/r_resist;
    _current = r_curent;
    // Ширина спирали mm (за шаг принято сечение провода)
    CGFloat r_cowidth = self.wiresInCoil * (self.wireDiameter*self.coilType) * self.turnsNumber;
    _coilLenght = r_cowidth;
    // Power density (Поверхностная мощность) W/mm²
    CGFloat r_powden = r_power / ((M_PI*2) * ((av_diam/2) * ((self.wireDiameter*2*(self.coilsNumber*self.wiresInCoil*1.8)) * self.turnsNumber)));
    _powerDensity = r_powden;
    //
    CGFloat mm_ras = (((M_PI*2) * ((av_diam/2) * ((self.wireDiameter*2*(self.coilsNumber*self.wiresInCoil*1.8)) * self.turnsNumber)))) * 0.3;
    
    CGFloat koef = (43 - mm_ras) / 100;
    if (koef <= 0.2) { koef = 0.2; }
    
    CGFloat den_kon = koef;
    CGFloat den_low = koef - 0.05;
    CGFloat den_hea = koef + 0.05;
    CGFloat den_ovh = koef + 0.1;
    
    
    // Opt Power (оптимальная мощность)
    CGFloat r_optpower = ((M_PI*2) * ((av_diam/2) * ((self.wireDiameter*2*(self.coilsNumber*self.wiresInCoil*1.8)) * self.turnsNumber))) * den_kon;
    _powerRecomender = r_optpower;
    
    // Температура спирали К (сухая) отдельно считаем сопротивление и ток для одной спирали
    CGFloat a_resist = (self.wireType * (r_wirelength+(self.wiresInCoil*self.wireDiameter)) / r_area / 1000) / (self.wiresInCoil);
    if (self.ohmCorrection == 1) {
        a_resist = a_resist * OHM_CORRECT;
    }
    CGFloat a_curent = self.voltage/a_resist;
    
    
    CGFloat r_tempk = pow( (self.voltage * a_curent) / (0.31 * M_PI * 5.67 * pow(10, -8) * self.wireDiameter * pow(10, -3) * r_wirelength * pow(10, -3)), 0.25f );
    _tempDry = r_tempk;
    // Температура спирали C (сухая)
    CGFloat r_tempc = r_tempk - 273.15;
    
    // Энергия J (Джоуль) Вт = Дж/с = кг·м²/с³
    CGFloat r_enerj = r_power;
    
    // Испарим жидкости за минуту
    CGFloat r_vapeliq = 60 / (r_enerj / 631.8) / 10000;
    
    // Рекомендуемые значения мощности
    //    CGFloat cool_hot = "Optimal";
    //    CGFloat cool_hot_col = "rgb(0,200,0)";
    //    if (r_powden >= 0.40) {
    //        CGFloat cool_hot = "Heat";
    //        CGFloat cool_hot_col = "rgb(200,180,00)";
    //    }
    //    if (r_powden >= 0.45) {
    //        CGFloat cool_hot = "Overheat";
    //        CGFloat cool_hot_col = "rgb(200,0,70)";
    //    }
    //    if (r_powden <= 0.2) {
    //        CGFloat cool_hot = "Low";
    //        CGFloat cool_hot_col = "rgb(100,100,200)";
    //    }
}



@end
