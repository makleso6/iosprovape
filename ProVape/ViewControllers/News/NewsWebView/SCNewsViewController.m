//
//  SCNewsViewController.m
//  ProVape
//
//  Created by Maxim Kolesnik on 02.11.15.
//  Copyright © 2015 Sugar And Candy LLC. All rights reserved.
//

#import "SCNewsViewController.h"

@interface SCNewsViewController ()

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation SCNewsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    NSURL *rssUrl = [NSURL URLWithString:@"http://vapenews.ru/product-innovations/2015/11/01/ipower-70w-tc-by-laisimo---interesnyy-novichok.html"];
//    
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:rssUrl];
//    
//    [self.webView loadRequest:request];
    
    [[NSBundle mainBundle] pathForResource:@"index" ofType:@"html"];
    
    NSURL *URL = [NSURL URLWithString:[[NSBundle mainBundle] pathForResource:@"index" ofType:@"html"]];
    
    NSLog(@"%@",URL);
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    
    request.cachePolicy = NSURLRequestReloadIgnoringCacheData;
    
    [self.webView loadRequest:request];
    
//    AFHTTPRequestOperation *op = [[AFHTTPRequestOperation alloc] initWithRequest:request];
//    
//    op.responseSerializer = [AFHTTPResponseSerializer serializer];
//    
//    [op setCompletionBlockWithSuccess:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
//        NSLog(@"%@",[[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
//    } failure:^(AFHTTPRequestOperation * _Nonnull operation, NSError * _Nonnull error) {
//        NSLog(@"%@",error);
//
//    }];
//    
//    [op start];
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
