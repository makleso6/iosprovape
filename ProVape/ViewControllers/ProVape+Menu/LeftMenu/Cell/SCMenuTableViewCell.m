//
//  SCMenuTableViewCell.m
//  Coil Pro
//
//  Created by Maxim Kolesnik on 19.10.15.
//  Copyright © 2015 Sugar And Candy LLC. All rights reserved.
//

#import "SCMenuTableViewCell.h"

@implementation SCMenuTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
