//
//  SCMenuTableViewCell.h
//  Coil Pro
//
//  Created by Maxim Kolesnik on 19.10.15.
//  Copyright © 2015 Sugar And Candy LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SCMenuTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *menuLabel;

@property (weak, nonatomic) IBOutlet UIImageView *menuImageView;

@end
