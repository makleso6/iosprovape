//
//  SCLeftMenuViewController.m
//  Coil Pro
//
//  Created by Maxim Kolesnik on 19.10.15.
//  Copyright © 2015 Sugar And Candy LLC. All rights reserved.
//

#import "SCLeftMenuViewController.h"
#import "SCMenuTableViewCell.h"

//#import "UIImage+ImageEffects.h"

@interface SCLeftMenuViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIImageView *profileImage;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *ratingLabel;

@end

@implementation SCLeftMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView setSeparatorColor:[UIColor blackColor]];
    
    self.profileImage.layer.cornerRadius = self.profileImage.frame.size.height/2;
    self.profileImage.layer.masksToBounds = YES;

    //CGRectGetWidth(self.profileImage.frame)/2;
    self.profileImage.layer.borderWidth = 2;
    self.profileImage.layer.borderColor = [UIColor lightTextColor].CGColor;
    // Do any additional setup after loading the view.
    
    //[self.tableView registerClass:[SCMenuTableViewCell class] forCellReuseIdentifier:@"menuCell"];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SCMenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"menuCell"];
    switch (indexPath.row) {
        case 0:{
            cell.menuLabel.text = [NSString stringWithFormat:NSLocalizedString(@"COIL_CALCULATOR", nil)];
            cell.menuImageView.image = [UIImage imageNamed:@"img_coil_calculator"];
            break;
        }
        case 1:{
            cell.menuLabel.text = [NSString stringWithFormat:NSLocalizedString(@"LIQUIDS_RECIPES", nil)];
            cell.menuImageView.image = [UIImage imageNamed:@"img_liquid_recipes"];

            break;
        }
        case 2:{
            cell.menuLabel.text = [NSString stringWithFormat:NSLocalizedString(@"FLUID_CALCULATOR", nil)];
            //img_calculator
            cell.menuImageView.image = [UIImage imageNamed:@"img_fluid_calculator"];

            break;
        }
        case 3:{
            cell.menuLabel.text = [NSString stringWithFormat:NSLocalizedString(@"MY_FLAVORS", nil)];
            cell.menuImageView.image = [UIImage imageNamed:@"img_my_flavors"];

            break;
        }
        case 5:{
            cell.menuLabel.text = [NSString stringWithFormat:NSLocalizedString(@"NEWS", nil)];
            cell.menuImageView.image = [UIImage imageNamed:@"img_news"];


            break;
        }
        case 6:{
            cell.menuLabel.text = [NSString stringWithFormat:NSLocalizedString(@"VIDEO", nil)];
            cell.menuImageView.image = [UIImage imageNamed:@"img_video"];


            break;
        }
        case 4:{
            cell.menuLabel.text = [NSString stringWithFormat:NSLocalizedString(@"SETTINGS", nil)];
            cell.menuImageView.image = [UIImage imageNamed:@"img_settings"];


            break;
        }
        default:
            break;
    }
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor pv_lightBlackColor];
    
    [cell setSelectedBackgroundView:bgColorView];
    return cell;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 56;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}




@end
