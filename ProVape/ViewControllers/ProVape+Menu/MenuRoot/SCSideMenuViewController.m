//
//  SCSideMenuViewController.m
//  Coil Pro
//
//  Created by Maxim Kolesnik on 19.10.15.
//  Copyright © 2015 Sugar And Candy LLC. All rights reserved.
//

#import "SCSideMenuViewController.h"

@interface SCSideMenuViewController ()

@end

@implementation SCSideMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)awakeFromNib
{
    self.menuPreferredStatusBarStyle = UIStatusBarStyleLightContent;
    self.contentViewShadowColor = [UIColor blackColor];
    self.contentViewShadowOffset = CGSizeMake(0, 0);
    self.contentViewShadowOpacity = 0.6;
    self.contentViewShadowRadius = 12;
    self.contentViewShadowEnabled = NO;
    self.fadeMenuView = NO;

    self.panFromEdge = YES;
    
    //self.fadeMenuView = NO;
    self.scaleContentView = NO;
    self.scaleBackgroundImageView = NO;
    self.scaleMenuView = NO;
    
    self.parallaxEnabled = NO;
    
    //self.parallaxContentMaximumRelativeValue = -30;
    self.contentViewInPortraitOffsetCenterX = 100;//self.view.frame.size.width;
    
    UIStoryboard *board = [UIStoryboard pv_getCoilStoryboard];
    
    UIViewController *coilViewController = [board instantiateInitialViewController];
    
    self.contentViewController = coilViewController;//[self.storyboard instantiateViewControllerWithIdentifier:@"contentViewController"];
    self.leftMenuViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"leftMenuViewController"];
    
    //self.rightMenuViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"rightMenuViewController"];
    //self.backgroundImage = [UIImage imageNamed:@"Stars"];
    //self.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
