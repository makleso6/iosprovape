//
//  SCSideMenuViewController.h
//  Coil Pro
//
//  Created by Maxim Kolesnik on 19.10.15.
//  Copyright © 2015 Sugar And Candy LLC. All rights reserved.
//

#import <RESideMenu/RESideMenu.h>

@interface SCSideMenuViewController : RESideMenu

@end
