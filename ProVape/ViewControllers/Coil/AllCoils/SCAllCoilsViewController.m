//
//  SCAllCoilsViewController.m
//  ProVape
//
//  Created by Maxim Kolesnik on 01.11.15.
//  Copyright © 2015 Sugar And Candy LLC. All rights reserved.
//

#import "SCAllCoilsViewController.h"
#import "SCCoilCell.h"


typedef NS_ENUM(NSUInteger, SCAllCoilsViewControllerType) {
    SCAllCoilsViewControllerTypeMyCoils,
    SCAllCoilsViewControllerTypeAllCoils
};

@interface SCAllCoilsViewController () <UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate>

- (IBAction)segementControll:(UISegmentedControl *)sender;

@property (weak, nonatomic) IBOutlet UISegmentedControl *coilsSegmentControl;

@property (nonatomic, strong) NSArray *data;


@end

@implementation SCAllCoilsViewController

-(PFQuery *)queryForTable {
    PFQuery *query = [super queryForTable];
    
    return query;
}


- (void)viewDidLoad {
    [super viewDidLoad];
        
    self.view.backgroundColor = [UIColor pv_darkBlackColor];
    
    //self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.backgroundView.backgroundColor = [UIColor clearColor];

    
    [self fetchData];
    PFQueryTableViewController *vc = [[PFQueryTableViewController alloc]init];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.refreshControl = [[UIRefreshControl alloc]init];
    [self.tableView addSubview:self.refreshControl];
    [self.refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
}

-(void)fetchData {
    self.data = [NSArray array];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", PFCoilRelationships.user, [PFUser currentUser]];

    PFQuery *query = [PFQuery queryWithClassName:[PFCoil parseClassName]];
    
    query.limit = 1000;

    
    
    [query findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
        NSLog(@"%lu", [objects count]);
        
        [PFObject pinAllInBackground:objects block:^(BOOL succeeded, NSError * _Nullable error) {
            
            //[query fromLocalDatastore];
            
            PFQuery *query2;// = [PFQuery queryWithClassName:[PFCoil parseClassName]];
            
            switch (self.coilsSegmentControl.selectedSegmentIndex) {
                case 0:{
                    query2 = [PFQuery queryWithClassName:[PFCoil parseClassName] predicate:predicate];
                    break;
                }
                case 1:{
                    query2 = [PFQuery queryWithClassName:[PFCoil parseClassName]];
                    break;
                }
                default:
                    break;
            }
            [query2 fromLocalDatastore];

            query2.limit = 1000;

            
            [query2 findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
                NSLog(@"%lu", [objects count]);
                self.data = [NSArray arrayWithArray:objects];
                [self.refreshControl endRefreshing];

                [self.tableView reloadData];

                
            }];
            
        }];
        
    }];

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)refreshTable {

    [self fetchData];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    const NSInteger numberOfObjects = [self.data count];
    return numberOfObjects;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SCCoilCell *cell = [tableView dequeueReusableCellWithIdentifier:SCCoilCellAttributes.reusableIdentifier];
    
    
    return cell;
}

-(void)configureCell:(SCCoilCell*)cell atIndexPath:(NSIndexPath *)indexPath {

    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}



- (IBAction)segementControll:(UISegmentedControl *)sender {
    [self fetchData];
    //[self.tableView reloadData];
    NSLog(@"%ld",(long)sender.selectedSegmentIndex);
    
}

#pragma mark - Deprecated

//#pragma mark - Table view data source
//
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    id<NSFetchedResultsSectionInfo> sectionInfo = self.fetchedResultsController.sections[section];
//    NSInteger numberOfObjects = [sectionInfo numberOfObjects];
//    return numberOfObjects;
//}
//
//
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    SCCoilCell *cell = [tableView dequeueReusableCellWithIdentifier:@"coil" forIndexPath:indexPath];
//    
//    [self configureCell:cell atIndexPath:indexPath];
//    
//    return cell;
//}
//
//-(void)configureCell:(SCCoilCell*)cell atIndexPath:(NSIndexPath *)indexPath {
//    Coil *coil = [self.fetchedResultsController objectAtIndexPath:indexPath];
//    
//    [cell configureWithCoil:coil];
//    
//}
//
//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    
//    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
//}
//
//
//
//- (IBAction)segementControll:(UISegmentedControl *)sender {
//    self.fetchedResultsController = nil;
//    
//    [self.tableView reloadData];
//    NSLog(@"%ld",(long)sender.selectedSegmentIndex);
//    
//}


//#pragma mark - NSFetchedResultsControllerDelegate
//
//- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
//    [self.tableView beginUpdates];
//    
//    self.deletedSections = [[NSMutableIndexSet alloc] init];
//    self.insertedSections = [[NSMutableIndexSet alloc] init];
//}
//
//- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
//    [self.tableView endUpdates];
//}
//
//- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id<NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
//    NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:sectionIndex];
//    
//    switch(type) {
//        case NSFetchedResultsChangeDelete:
//            [self.tableView deleteSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
//            [self.deletedSections addIndexes:indexSet];
//            break;
//            
//        case NSFetchedResultsChangeInsert:
//            [self.tableView insertSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
//            [self.insertedSections addIndexes:indexSet];
//            break;
//            
//        default:
//            break;
//    }
//}
//
//- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
//    switch(type) {
//        case NSFetchedResultsChangeDelete:
//            [self.tableView deleteRowsAtIndexPaths:@[ indexPath ] withRowAnimation:UITableViewRowAnimationAutomatic];
//            break;
//            
//        case NSFetchedResultsChangeInsert:
//            [self.tableView insertRowsAtIndexPaths:@[ newIndexPath ] withRowAnimation:UITableViewRowAnimationAutomatic];
//            break;
//            
//        case NSFetchedResultsChangeMove:
//            // iOS 9.0b5 sends the same index path twice instead of delete
//            if(![indexPath isEqual:newIndexPath]) {
//                [self.tableView deleteRowsAtIndexPaths:@[ indexPath ] withRowAnimation:UITableViewRowAnimationAutomatic];
//                [self.tableView insertRowsAtIndexPaths:@[ newIndexPath ] withRowAnimation:UITableViewRowAnimationAutomatic];
//            }
//            else if([self.insertedSections containsIndex:indexPath.section]) {
//                // iOS 9.0b5 bug: Moving first item from section 0 (which becomes section 1 later) to section 0
//                // Really the only way is to delete and insert the same index path...
//                [self.tableView deleteRowsAtIndexPaths:@[ indexPath ] withRowAnimation:UITableViewRowAnimationAutomatic];
//                [self.tableView insertRowsAtIndexPaths:@[ indexPath ] withRowAnimation:UITableViewRowAnimationAutomatic];
//            }
//            else if([self.deletedSections containsIndex:indexPath.section]) {
//                // iOS 9.0b5 bug: same index path reported after section was removed
//                // we can ignore item deletion here because the whole section was removed anyway
//                [self.tableView insertRowsAtIndexPaths:@[ indexPath ] withRowAnimation:UITableViewRowAnimationAutomatic];
//            }
//            
//            break;
//            
//        case NSFetchedResultsChangeUpdate:
//            // On iOS 9.0b5 NSFetchedResultsController may not even contain such indexPath anymore
//            // when removing last item from section.
//            if(![self.deletedSections containsIndex:indexPath.section] && ![self.insertedSections containsIndex:indexPath.section]) {
//                // iOS 9.0b5 sends update before delete therefore we cannot use reload
//                // this will never work correctly but at least no crash.
//                UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
//                [self configureCell:cell atIndexPath:indexPath];
//            }
//            
//            break;
//    }
//}
@end
