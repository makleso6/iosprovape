//
//  SCCoilCell.m
//  ProVape
//
//  Created by Maxim Kolesnik on 01.11.15.
//  Copyright © 2015 Sugar And Candy LLC. All rights reserved.
//

#import "SCCoilCell.h"
#import "User.h"
#import "SCContainerCell.h"

const struct SCCoilCellAttributes SCCoilCellAttributes = {
    .reusableIdentifier = @"SCCoilCell",
    .estimatedHeight = 44
};

@implementation SCCoilCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Configuration

- (void)configureWithContainer:(SCContainerCell *)container {
    
    self.nameLabel.text = container.name;
    
    self.authorLabel.text = container.author;
    
    self.resistanceLabel.text = [NSString stringWithFormat:@"%@ Ω", container.resistanse];
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor pv_lightBlackColor];
    
    [self setSelectedBackgroundView:bgColorView];
    
}


- (void)configureWithCoil:(Coil *)coil {
        
    self.nameLabel.text = coil.identifier;
    
    self.authorLabel.text = coil.user.userName;

    self.resistanceLabel.text = [NSString stringWithFormat:@"%.2f Ω", [coil.resistance floatValue]];
    
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor pv_grayColor];
    
    [self setSelectedBackgroundView:bgColorView];

}

@end
