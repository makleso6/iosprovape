//
//  SCCoilCell.h
//  ProVape
//
//  Created by Maxim Kolesnik on 01.11.15.
//  Copyright © 2015 Sugar And Candy LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#define SC_DEPRECATED __attribute__((deprecated))

extern const struct SCCoilCellAttributes {
    __unsafe_unretained NSString *reusableIdentifier;
    CGFloat estimatedHeight;
} SCCoilCellAttributes;

@class SCContainerCell;

@interface SCCoilCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *resistanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *authorLabel;

- (void)configureWithContainer:(SCContainerCell *)container;


- (void)configureWithCoil:(Coil *)coil SC_DEPRECATED;

@end
