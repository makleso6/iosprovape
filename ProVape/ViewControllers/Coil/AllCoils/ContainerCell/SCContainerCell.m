//
//  SCContainerCell.m
//  ProVape
//
//  Created by Maxim Kolesnik on 20.11.15.
//  Copyright © 2015 Sugar And Candy LLC. All rights reserved.
//

#import "SCContainerCell.h"

@implementation SCContainerCell


- (instancetype)initWithName:(NSString *)name withResistanse:(NSString *)resistanse withAuthor:(NSString *)author {
    self = [super init];
    if (self) {
        self.name = name;
        self.resistanse = resistanse;
        self.author = author;
    }
    return self;
}

+ (instancetype)containerWithName:(NSString *)name withResistanse:(NSString *)resistanse withAuthor:(NSString *)author {
	SCContainerCell *container = [[SCContainerCell alloc]initWithName:name
                                                       withResistanse:resistanse
                                                           withAuthor:author];

    return container;
}
@end
