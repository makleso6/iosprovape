//
//  SCContainerCell.h
//  ProVape
//
//  Created by Maxim Kolesnik on 20.11.15.
//  Copyright © 2015 Sugar And Candy LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SCContainerCell : NSObject

- (instancetype)initWithName:(NSString *)name withResistanse:(NSString *)resistanse withAuthor:(NSString *)author;
+ (instancetype)containerWithName:(NSString *)name withResistanse:(NSString *)resistanse withAuthor:(NSString *)author;


@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *resistanse;
@property (nonatomic, strong) NSString *author;

@end
