//
//  SCCreateCoilViewController.m
//  ProVape
//
//  Created by Maxim Kolesnik on 04.11.15.
//  Copyright © 2015 Sugar And Candy LLC. All rights reserved.
//

#import "SCCreateCoilViewController.h"
#import "SCBatteryCell.h"
#import "SCWiresInCoilCell.h"
#import "SCResultCoilCell.h"
#import "SCCoilTypeCell.h"


#define WIRE_OPTIONS_ROW_NUMBER 0
#define COIL_OPTIONS_ROW_NUMBER 1
#define TURNS_OPTIONS_ROW_NUMBER 2
#define BATTERY_OPTIONS_ROW_NUMBER 3
#define RESULT_OPTIONS_ROW_NUMBER 4

#define BATTERY_CELL ((SCBatteryCell *) cell)
#define COIL_TYPE_CELL ((SCCoilTypeCell *) cell)
#define WIRES_IN_COIL_CELL ((SCWiresInCoilCell *) cell)
#define RESULT_CELL ((SCResultCoilCell *) cell)

/*<td style="vertical-align:top;">
	<select id="wire_type" onchange="return compute(this);">
 <option value="0.09">Nickel 200 (Ni)</option>
 <option value="0.24">NiFe30</option>
 <option value="0.28">Dicodes Resist</option>
 <option value="0.36">NiFe48</option>
 <option value="0.42">Titanium (Ti)</option>
 <option value="0.74">SS AISI 316</option>
 <option value="0.8">SS AISI 304</option>
 <option value="1.08">Nichrome Ni80</option>
 <option value="1.11">Nichrome Ni60</option>
 <option value="1.39">Фехраль</option>
 <option value="1.35">Kanthal D</option>
 <option value="1.45" selected="">Kanthal A1</option>
	</select>
	
	<span class="mini_angl">TCR:</span> <span id="tc" class="mini_angl">0.00001</span>
	
	</td>*/
NSString * const wireType[] = {
    @"Nickel 200 (Ni)",
    @"NiFe30",
    @"Dicodes Resist",
    @"NiFe48",
    @"Titanium (Ti)",
    @"SS AISI 316",
    @"SS AISI 304",
    @"Nichrome Ni80",
    @"Nichrome Ni60",
    @"Fehral",
    @"Kanthal D",
    @"Kanthal A1",
};

NSString * const wireLengsLength[] = {
    @"2x0 mm",
    @"2x0.5 mm",
    @"2x1 mm",
    @"2x1.5 mm",
    @"2x2 mm",
    @"2x2.5 mm",
    @"2x3 mm",
    @"2x4 mm",
    @"2x5 mm",
    
};

NSString * const coilDiameter[] = {
    @"0.5",
    @"0.6",
    @"0.7",
    @"0.8",
    @"0.9",
    @"1",
    @"1.25",
    @"1.5",
    @"1.75",
    @"2",
    @"2.25",
    @"2.75",
    @"3",
    @"3.5",
    @"4"
};

NSString * const wireDiametres[] = {
    @"0.10 mm (AWG 38)",
    @"0.11 mm (AWG 37)",
    @"0.12 mm (AWG 36)",
    @"0.14 mm (AWG 35)",
    @"0.15 mm",
    @"0.16 mm (AWG 34)",
    @"0.18 mm (AWG 33)",
    @"0.20 mm (AWG 32)",
    @"0.22 mm (AWG 31)",
    @"0.25 mm (AWG 30)",
    @"0.25 mm (AWG 30)",
    @"0.28 mm (AWG 29)",
    @"0.30 mm",
    @"0.32 mm (AWG 28)",
    @"0.35 mm",
    @"0.36 mm (AWG 27)",
    @"0.38 mm",
    @"0.40 mm (AWG 26)",
    @"0.45 mm (AWG 25)",
    @"0.50 mm",
    @"0.51 mm (AWG 24)",
    @"0.57 mm (AWG 23)",
    @"0.63 mm",
    @"0.64 mm (AWG 22)",
    @"0.70 mm",
    @"0.71 mm (AWG 21)",
    @"0.80 mm",
    @"0.81 mm (AWG 20)",
    @"0.90 mm",
    @"0.91 mm (AWG 19)",
    @"1.0 mm",
    @"1.02 mm (AWG 18)"
};
@interface SCCreateCoilViewController () <PickerCellsDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) PickerCellsController *pickersController;

@property (nonatomic, strong) UIPickerView *wiresInCoilpickerView;
@property (nonatomic, strong) UIPickerView *coilNumberPickerView;
@property (nonatomic, strong) UIPickerView *wireDiameterPickerView;
@property (nonatomic, strong) UIPickerView *coilDiameterPickerView;
@property (nonatomic, strong) UIPickerView *turnNumberPickerView;
@property (nonatomic, strong) UIPickerView *legsLengthPickerView;
@property (nonatomic, strong) UIPickerView *wireTypePickerView;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, assign) BOOL twisted;
@property (nonatomic, assign) BOOL ohmCorrected;

@property (nonatomic, assign) SCCoilCalculatorCoilType coilType;

@property (nonatomic, assign) CGFloat voltage;

@end

@implementation SCCreateCoilViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.pickersController = [[PickerCellsController alloc] init];
    [self.pickersController attachToTableView:self.tableView tableViewsPriorDelegate:self withDelegate:self];
    
    
    self.wiresInCoilpickerView = [[UIPickerView alloc] init];
    self.wiresInCoilpickerView.tag = 0;
    self.wiresInCoilpickerView.delegate = self;
    self.wiresInCoilpickerView.dataSource = self;
    NSIndexPath *wiresInCoilpickerIP = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.wiresInCoilpickerView selectRow:0 inComponent:0 animated:YES];
    [self.pickersController addPickerView:self.wiresInCoilpickerView forIndexPath:wiresInCoilpickerIP];
    
    self.coilNumberPickerView = [[UIPickerView alloc] init];
    self.coilNumberPickerView.tag = 1;
    self.coilNumberPickerView.delegate = self;
    self.coilNumberPickerView.dataSource = self;
    NSIndexPath *coilNumberPickerIP = [NSIndexPath indexPathForRow:0 inSection:1];
    [self.coilNumberPickerView selectRow:4 inComponent:0 animated:YES];
    [self.pickersController addPickerView:self.coilNumberPickerView forIndexPath:coilNumberPickerIP];
    
    
    self.wireDiameterPickerView = [[UIPickerView alloc] init];
    self.wireDiameterPickerView.tag = 2;
    self.wireDiameterPickerView.backgroundColor = [UIColor clearColor];
    self.wireDiameterPickerView.delegate = self;
    self.wireDiameterPickerView.dataSource = self;
    NSIndexPath *wireDiameterPickerIP = [NSIndexPath indexPathForRow:1 inSection:0];
    [self.wireDiameterPickerView selectRow:13 inComponent:0 animated:YES];
    [self.pickersController addPickerView:self.wireDiameterPickerView forIndexPath:wireDiameterPickerIP];
    
    self.coilDiameterPickerView = [[UIPickerView alloc] init];
    self.coilDiameterPickerView.tag = 3;
    self.coilDiameterPickerView.delegate = self;
    self.coilDiameterPickerView.dataSource = self;
    NSIndexPath *coilDiameterPickerIP = [NSIndexPath indexPathForRow:0 inSection:2];
    [self.coilDiameterPickerView selectRow:9 inComponent:0 animated:YES];
    [self.pickersController addPickerView:self.coilDiameterPickerView forIndexPath:coilDiameterPickerIP];
    
    self.turnNumberPickerView = [[UIPickerView alloc] init];
    self.turnNumberPickerView.tag = 4;
    self.turnNumberPickerView.delegate = self;
    self.turnNumberPickerView.dataSource = self;
    NSIndexPath *turnNumberPickerIP = [NSIndexPath indexPathForRow:1 inSection:2];
    [self.turnNumberPickerView selectRow:12 inComponent:0 animated:YES];
    [self.pickersController addPickerView:self.turnNumberPickerView forIndexPath:turnNumberPickerIP];
    
    self.legsLengthPickerView = [[UIPickerView alloc] init];
    self.legsLengthPickerView.tag = 5;
    self.legsLengthPickerView.delegate = self;
    self.legsLengthPickerView.dataSource = self;
    NSIndexPath *legsLengthPickerIP = [NSIndexPath indexPathForRow:3 inSection:0];
    [self.legsLengthPickerView selectRow:2 inComponent:0 animated:YES];
    [self.pickersController addPickerView:self.legsLengthPickerView forIndexPath:legsLengthPickerIP];
    
    self.wireTypePickerView = [[UIPickerView alloc] init];
    self.wireTypePickerView.tag = 6;
    self.wireTypePickerView.delegate = self;
    self.wireTypePickerView.dataSource = self;
    NSIndexPath *wireTypePickerIP = [NSIndexPath indexPathForRow:2 inSection:0];
    [self.wireTypePickerView selectRow:11 inComponent:0 animated:YES];
    [self.pickersController addPickerView:self.wireTypePickerView forIndexPath:wireTypePickerIP];
    
    
    self.coilType = 1;
    self.ohmCorrected = YES;
    self.twisted = NO;
    
    [self setupNavigationBar];
}

- (void)setupNavigationBar {
    UIBarButtonItem *done = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(done:)];
    [done setTitleTextAttributes:@{NSFontAttributeName : [UIFont pv_AvenurNextBig],
                                   NSForegroundColorAttributeName : [UIColor pv_whiteColor]}
                        forState:UIControlStateNormal];
    
    [self.navigationItem setRightBarButtonItem:done
                                     animated:YES];
    self.title = NSLocalizedString(@"CREATE_COIL_TITLE", nil);
}

-(void)done:(UIBarButtonItem*)sender {
    NSLog(@"done");
    
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        Coil *coil = [Coil MR_createEntityInContext:localContext];
        
        SCCoilCalculator *calculator = [SCCoilCalculator sharedInstance];
        
      
        
        coil.wiresInCoil = @(calculator.wiresInCoil);
        coil.wireDiameter = @(calculator.wireDiameter);
        coil.wireType = @(calculator.wireType);
        coil.wireLengsLength = @(calculator.wireLengsLength);
        
        coil.coilsNumber = @(calculator.coilsNumber);
        coil.coilType = @(calculator.coilType);
        
        coil.turnDiameter = @(calculator.turnDiameter);
        coil.turnsNumber = @(calculator.turnsNumber);
        
        coil.resistance = @(calculator.resistance);
        
        User *user = [User MR_findFirstByAttribute:BaseAttributes.identifier withValue:[PFUser currentUser].objectId inContext:localContext];
        
        NSLog(@"%@",user.identifier);
        
        if (user) {
            coil.user = user;

        }
        
        

    } completion:^(BOOL contextDidSave, NSError *error) {
        [self.navigationController popViewControllerAnimated:YES];
    }];
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
    NSLog(@"%ld",(long)pickerView.tag);
    if ([pickerView isEqual:self.wiresInCoilpickerView]) {
        return 6;
    }
    if ([pickerView isEqual:self.coilNumberPickerView]) {
        return 4;
    }
    if ([pickerView isEqual:self.wireDiameterPickerView]) {
        return 32;
    }
    if ([pickerView isEqual:self.coilDiameterPickerView]) {
        return 15;
    }
    if ([pickerView isEqual:self.turnNumberPickerView]) {
        return 40;
    }
    if ([pickerView isEqual:self.legsLengthPickerView]) {
        return 9;
    }
    if ([pickerView isEqual:self.wireTypePickerView]) {
        return 12;
    }
    return 0;
    
}

- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont pv_AvenurNextDefault],
                                 NSForegroundColorAttributeName : [UIColor pv_whiteColor]};
    NSString *string;
    if ([pickerView isEqual:self.wiresInCoilpickerView]) {
        string = [NSString stringWithFormat:@"%ld", (long)row+1];;
        
    }
    if ([pickerView isEqual:self.coilNumberPickerView]) {
        string = [NSString stringWithFormat:@"%ld", (long)row+1];;
    }
    if ([pickerView isEqual:self.wireDiameterPickerView]) {
        string = [NSString stringWithFormat:@"%@", wireDiametres[row]];
    }
    if ([pickerView isEqual:self.coilDiameterPickerView]) {
        string = [NSString stringWithFormat:@"%@", coilDiameter[row]];
    }
    if ([pickerView isEqual:self.turnNumberPickerView]) {
        if ((row+1) % 2 == 0) {
            string = [NSString stringWithFormat:@"%ld ½", row/2+1];
            
        } else {
            string = [NSString stringWithFormat:@"%ld", (long)row/2+1];
        }
    }
    if ([pickerView isEqual:self.legsLengthPickerView]) {
        string = [NSString stringWithFormat:@"%@",wireLengsLength[row]];
    }
    if ([pickerView isEqual:self.wireTypePickerView]) {
        string = [NSString stringWithFormat:@"%@", wireType[row]];
    }
    
    [[pickerView.subviews objectAtIndex:1] setBackgroundColor:[UIColor pv_whiteColor]];
    [[pickerView.subviews objectAtIndex:2] setBackgroundColor:[UIColor pv_whiteColor]];
    
    return [[NSAttributedString alloc]initWithString:string attributes:attributes];
    
    
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    NSIndexPath *ip = [self.pickersController indexPathForPicker:pickerView];
    if (ip) {
        [self.tableView reloadRowsAtIndexPaths:@[ip] withRowAnimation:UITableViewRowAnimationFade];
        [self reCalculate];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    static NSInteger numberOfSections = 5;
    return numberOfSections;
}



-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    switch (section) {
        case WIRE_OPTIONS_ROW_NUMBER:{
            return 4;
            
            break;
        }
        case COIL_OPTIONS_ROW_NUMBER:{
            return 2;
            
            break;
        }
        case TURNS_OPTIONS_ROW_NUMBER:{
            return 2;
            
            break;
        }
        case BATTERY_OPTIONS_ROW_NUMBER:{
            return 1;
            
            break;
        }
        case RESULT_OPTIONS_ROW_NUMBER:{
            return 1;
            
            break;
        }
        default:
            
            break;
    }
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString * paramsCell  = @"paramsCell";
    
    static NSString * batteryCell = @"batteryCell";
    
    static NSString * resultCell  = @"resultCell";
    
    static NSString * coilTypeCell  = @"coilTypeCell";
    
    static NSString * wiresCell = @"wiresCell";
    NSString *title;
    
    id picker = [self.pickersController pickerForOwnerCellIndexPath:indexPath];
    
    if ([picker isKindOfClass:[UIPickerView class]]) {
        UIPickerView *pickerView = (UIPickerView *)picker;
        NSInteger selectedRow = [pickerView selectedRowInComponent:0];
        title = [self pickerView:pickerView attributedTitleForRow:selectedRow forComponent:0].string;
    }
    
    UITableViewCell *cell ;
    switch (indexPath.section) {
        case 0:{
            switch (indexPath.row) {
                case 0:{
                    cell = [tableView dequeueReusableCellWithIdentifier:wiresCell forIndexPath:indexPath];
                    
                    cell.textLabel.text = [NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"WIRES_IN_COIL", nil),title];
                    
                    [WIRES_IN_COIL_CELL.twistedSwitch addTarget:self action:@selector(twistedSwitchAction:) forControlEvents:UIControlEventValueChanged];
                    
                    [WIRES_IN_COIL_CELL.twistedSwitch setOnTintColor:[UIColor pv_darkGrayColor]];
                    
                    WIRES_IN_COIL_CELL.twistedSwitch.on = self.twisted;
                    
                    if ([title integerValue] == 1) {
                        WIRES_IN_COIL_CELL.twistedSwitch.on = NO;
                        
                        WIRES_IN_COIL_CELL.twistedSwitch.enabled = NO;
                        
                        self.twisted = NO;
                    } else {
                        WIRES_IN_COIL_CELL.twistedSwitch.enabled = YES;
                        
                    }
                    
                    break;
                }
                case 1:{
                    cell = [tableView dequeueReusableCellWithIdentifier:paramsCell forIndexPath:indexPath];
                    
                    cell.textLabel.text = [NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"WIRE_DIAMETER", nil),title];
                    break;
                }
                case 2:{
                    cell = [tableView dequeueReusableCellWithIdentifier:paramsCell forIndexPath:indexPath];
                    
                    cell.textLabel.text = [NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"WIRES_TYPE", nil),title];
                    break;
                }
                case 3:{
                    cell = [tableView dequeueReusableCellWithIdentifier:paramsCell forIndexPath:indexPath];
                    
                    cell.textLabel.text = [NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"WIRE_LENGS_LENGTH", nil),title];
                    break;
                }
                default:
                    break;
            }
            break;
            
        }
            
        case 1:{
            
            switch (indexPath.row) {
                case 0:{
                    cell = [tableView dequeueReusableCellWithIdentifier:paramsCell forIndexPath:indexPath];
                    
                    cell.textLabel.text = [NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"COILS_NUMBER", nil),title];
                    break;
                }
                case 1:{
                    cell = [tableView dequeueReusableCellWithIdentifier:coilTypeCell forIndexPath:indexPath];
                    
                    COIL_TYPE_CELL.textLabel.text = [NSString stringWithFormat:@"%@",NSLocalizedString(@"COILS_TYPE", nil)];
                    [COIL_TYPE_CELL.coilTypeSegmentedControl addTarget:self action:@selector(changeCoilType:) forControlEvents:UIControlEventValueChanged];
                    break;
                }
                default:
                    break;
            }
            break;
            
        }
            
        case 2:{
            cell = [tableView dequeueReusableCellWithIdentifier:paramsCell forIndexPath:indexPath];
            
            switch (indexPath.row) {
                case 0:{
                    cell.textLabel.text = [NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"TURNS_DIAMETER", nil),title];
                    break;
                }
                case 1:{
                    cell.textLabel.text = [NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"TURNS_NUMBER", nil),title];
                    break;
                }
                    
                default:
                    break;
            }
            break;
            
        }
        case 3:{
            switch (indexPath.row) {
                case 0:{
                    cell = [tableView dequeueReusableCellWithIdentifier:batteryCell];
                    
                    BATTERY_CELL.totalOutputLabel.text = [NSString stringWithFormat:@"%.1f",BATTERY_CELL.voltageSlider.value];
                    
                    [BATTERY_CELL.voltageSlider addTarget:self action:@selector(changeVoltageAction:) forControlEvents:UIControlEventValueChanged];
                    [BATTERY_CELL.correctionSwitch addTarget:self action:@selector(correctionSwitchAction:) forControlEvents:UIControlEventValueChanged];
                    
                    [BATTERY_CELL.correctionSwitch setOnTintColor:[UIColor pv_darkGrayColor]];
                    
                    
                    break;
                }
                default:
                    //cell.textLabel.text = @"title";
                    break;
            }
            break;
            
        }
        case 4:{
            switch (indexPath.row) {
                case 0:{
                    
                    cell = [tableView dequeueReusableCellWithIdentifier:resultCell];
                    
                    SCCoilCalculator * calculator = [SCCoilCalculator sharedInstance];
                    
                    RESULT_CELL.powerLabel.text = [NSString stringWithFormat:@"%.2f Watt",calculator.currentWatts];
                    RESULT_CELL.recommendedLabel.text= [NSString stringWithFormat:@"%.2f Watt",calculator.powerRecomender];
                    RESULT_CELL.resistanceLabel.text= [NSString stringWithFormat:@"%.2f Ω",calculator.resistance];
                    RESULT_CELL.wireLengthLabel.text= [NSString stringWithFormat:@"%.2f mm x1",calculator.wireLenght];
                    RESULT_CELL.electricityLabel.text= [NSString stringWithFormat:@"%.2f Amp",calculator.current];
                    RESULT_CELL.poweDensityLabel.text= [NSString stringWithFormat:@"%.2f W/mm²",calculator.powerDensity];
                    RESULT_CELL.coilLengthLabel.text= [NSString stringWithFormat:@"%.2f mm",calculator.coilLenght];
                    RESULT_CELL.temperatureLabel.text= [NSString stringWithFormat:@"%.2f K",calculator.tempDry];

                    
                    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 169, self.view.frame.size.width, 1)];
                    v.backgroundColor = [UIColor clearColor];
                    [self.tableView setTableFooterView:v];
                    break;
                }
                default:
                    //cell.textLabel.text = @"title";
                    break;
            }
            break;
            
        }
            
        default:
            break;
    }
    
    cell.textLabel.textColor = [UIColor whiteColor];
    
    cell.textLabel.font = [UIFont pv_AvenurNextDefault];

    //[self reCalculate];
    return cell;
}







#pragma mark - UITableViewDelegate

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    NSString *sectionName;
    
    switch (section) {
        case 0:{
            sectionName = [NSString stringWithFormat:NSLocalizedString(@"WIRE_OPTIONS", nil)];
            break;
        }
        case 1:{
            sectionName = [NSString stringWithFormat:NSLocalizedString(@"COIL_OPTIONS", nil)];
            
            break;
        }
        case 2:{
            sectionName = [NSString stringWithFormat:NSLocalizedString(@"TURNS_OPTIONS", nil)];
            
            break;
        }
        case 3:{
            sectionName = [NSString stringWithFormat:NSLocalizedString(@"POWER_OPTIONS", nil)];
            
            break;
        }
        case 4:{
            sectionName = [NSString stringWithFormat:NSLocalizedString(@"RESULT", nil)];
            
            break;
        }
        default:
            
            break;
    }
    
    UIView *view = [[UIView alloc]init];
    
    UILabel *label = [[UILabel alloc]init];
    
    view.backgroundColor = [UIColor pv_darkBlackColor];
    
    label.text = sectionName;
    
    label.frame = CGRectMake(16, (60 - 44) / 2, 200, 44);
    
    label.textColor = [UIColor pv_darkGrayColor];
    
    label.font = [UIFont pv_AvenurNextSmall];
    
    [view addSubview:label];
    
    return view;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 60;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case 0:
        case 1:
        case 2:
            return 60;
            break;
        case 3:
            return 90;
            break;
        case 4:
            return 254;
            break;
        default:
            break;
    }
    
    return 60;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    BOOL returnedValue = YES;
    
    switch (indexPath.section) {
        case 1: {
            switch (indexPath.row) {
                case 0:{
                    returnedValue = YES;
                    
                    break;
                }
                case 1:{
                    returnedValue = NO;
                    break;
                }
                default:
                    
                    break;
            }
            break;
        }
        case 3: {
            returnedValue = NO;
            break;
        }
        case 4: {
            returnedValue = NO;
            break;
        }
        default: {
            break;
        }
    }
    return returnedValue;
}

#pragma mark - Class Methods

-(void)reCalculate {
    
    NSInteger wiresInCoil = [self.wiresInCoilpickerView selectedRowInComponent:0] + 1;
    NSLog(@"%ld", (long)wiresInCoil);
    
    CGFloat wireDiameter = [[wireDiametres[[self.wireDiameterPickerView selectedRowInComponent:0]] substringToIndex:4]floatValue];
    NSLog(@"%f", wireDiameter);
    
    CGFloat wireType = [self wireTypeWithIndex:[self.wireTypePickerView selectedRowInComponent:0]];
    NSLog(@"%f", wireType);
    
    CGFloat wireLengsLength = [self wireLengsLengthWithIndex:[self.legsLengthPickerView selectedRowInComponent:0]];
    NSLog(@"%f", wireLengsLength);
    
    NSInteger coilsNumber = [self.coilNumberPickerView selectedRowInComponent:0] + 1;
    NSLog(@"%ld", (long)coilsNumber);
    
    NSInteger coilType = self.coilType;
    NSLog(@"%ld", (long)coilType);
    
    CGFloat turnDiameter = [coilDiameter[[self.coilDiameterPickerView selectedRowInComponent:0]] floatValue];// = [self turnDiameterWithIndex:[self.turnNumberPickerView selectedRowInComponent:0]];
    NSLog(@"%f", turnDiameter);
    
    CGFloat turnsNumber = [self turnsNumberWithIndex:[self.turnNumberPickerView selectedRowInComponent:0]];
    NSLog(@"%f", turnsNumber);
    
    CGFloat voltage = self.voltage;
    NSLog(@"%f", voltage);
    
    BOOL twisted = self.twisted;
    NSLog(@"%d",twisted);
    
    BOOL ohmCorrected = self.ohmCorrected;
    NSLog(@"%d",ohmCorrected);
    
    NSArray *options = @[@(wiresInCoil),
                         @(coilsNumber),
                         @(wireDiameter),
                         @(turnDiameter),
                         @(turnsNumber),
                         @(wireLengsLength),
                         @(wireType),
                         @(coilType),
                         @(ohmCorrected),
                         @(twisted),
                         @(voltage)];
    
    SCCoilCalculator * calculator = [SCCoilCalculator sharedInstanceWithStandartSettings:options];
    
    NSLog(@"\ncurrentWatts: %f\nressitance: %f",calculator.currentWatts, calculator.resistance);
    
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForItem:0 inSection:4]] withRowAnimation:UITableViewRowAnimationNone];
}

-(CGFloat)turnsNumberWithIndex:(NSInteger)index {
    
    CGFloat turnDiameter;
    
    //return (index + 1) /2;
    
    if ((index+1) % 2 == 0) {
        
        turnDiameter = index/2+1 + 0.5;
        
    } else {
        turnDiameter = index/2+1;
    }
    
    return turnDiameter;

}

-(CGFloat)wireTypeWithIndex:(NSInteger)index {
    CGFloat wireType;
    switch (index) {
        case 0:{
            wireType = SCCoilCalculatorWireType.nickel200;
            break;
        }
        case 1:{
            wireType = SCCoilCalculatorWireType.NiFe30;
            break;
        }
        case 2:{
            wireType = SCCoilCalculatorWireType.dicodesResist;
            break;
        }
        case 3:{
            wireType = SCCoilCalculatorWireType.NiFe48;
            break;
        }
        case 4:{
            wireType = SCCoilCalculatorWireType.titanium;
            break;
        }
        case 5:{
            wireType = SCCoilCalculatorWireType.SSAISI316;
            break;
        }
        case 6:{
            wireType = SCCoilCalculatorWireType.SSAISI340;
            break;
        }
        case 7:{
            wireType = SCCoilCalculatorWireType.nichromeNi80;
            break;
        }
        case 8:{
            wireType = SCCoilCalculatorWireType.nichromeNi60;
            break;
        }
        case 9:{
            wireType = SCCoilCalculatorWireType.fechral;
            break;
        }
        case 10:{
            wireType = SCCoilCalculatorWireType.kanthalD;
            break;
        }
        case 11:{
            wireType = SCCoilCalculatorWireType.kanthalA1;
            break;
        }
        default:
            break;
    }
    return wireType;
}

-(CGFloat)wireLengsLengthWithIndex:(NSInteger)index {
    CGFloat wireLengsLength;
    switch (index) {
        case 0:{
            wireLengsLength = 0;
            break;
        }
        case 1:{
            wireLengsLength = 0.5f;
            break;
        }
        case 2:{
            wireLengsLength = 1.f;
            
            break;
        }
        case 3:{
            wireLengsLength = 1.5f;
            
            break;
        }
        case 4:{
            wireLengsLength = 2.f;
            
            break;
        }
        case 5:{
            wireLengsLength = 2.5f;
            
            break;
        }
        case 6:{
            wireLengsLength = 3.f;
            
            break;
        }
        case 7:{
            wireLengsLength = 4.f;
            
            break;
        }
        case 8:{
            wireLengsLength = 5.f;
            
            break;
        }
        default:
            break;
    }
    return wireLengsLength;
}

#pragma mark - Actions

-(void)twistedSwitchAction:(UISwitch *)sender {
    self.twisted = [sender isOn];
    [self reCalculate];
}

-(void)correctionSwitchAction:(UISwitch *)sender {
    self.ohmCorrected = [sender isOn];
    [self reCalculate];
    
}

- (void)changeCoilType:(UISegmentedControl *)sender {
    NSLog(@"%ld",(long)sender.selectedSegmentIndex);
    
    switch (sender.selectedSegmentIndex) {
        case 0:
            self.coilType = SCCoilCalculatorCoilTypeMicro;
            break;
        case 1:
            self.coilType = SCCoilCalculatorCoilTypeNormal;
            break;
            
        default:
            break;
    }
    [self reCalculate];

}

-(void)changeVoltageAction:(UISlider *)sender {
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:3];
    
    SCBatteryCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    
    cell.totalOutputLabel.text = [NSString stringWithFormat:@"%.1f",sender.value];
    
    self.voltage = sender.value;
    [self reCalculate];

    
}

@end