//
//  SCCoilTypeCell.h
//  ProVape
//
//  Created by Maxim Kolesnik on 13.11.15.
//  Copyright © 2015 Sugar And Candy LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SCCoilTypeCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UISegmentedControl *coilTypeSegmentedControl;


@end
