//
//  SCResultCell.h
//  ProVape
//
//  Created by Maxim Kolesnik on 12.11.15.
//  Copyright © 2015 Sugar And Candy LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SCResultCoilCell : UITableViewCell
/**
 *  DROPS
 */
@property (weak, nonatomic) IBOutlet UILabel *baseFluidDropsLabel;

@property (weak, nonatomic) IBOutlet UILabel *propylenGlycolDropsLabel;

@property (weak, nonatomic) IBOutlet UILabel *glycerinDropsLabel;

@property (weak, nonatomic) IBOutlet UILabel *waterDropsLabel;

@property (weak, nonatomic) IBOutlet UILabel *flavorsDropsLabel;

/**
 *  ML
 */
@property (weak, nonatomic) IBOutlet UILabel *baseFluidVolumeLabel;

@property (weak, nonatomic) IBOutlet UILabel *propylenGlycolVolumeLabel;

@property (weak, nonatomic) IBOutlet UILabel *glycerinVolumeLabel;

@property (weak, nonatomic) IBOutlet UILabel *waterVolumeLabel;

@property (weak, nonatomic) IBOutlet UILabel *flavorsVolumeLabel;


@property (weak, nonatomic) IBOutlet UILabel *powerLabel;
@property (weak, nonatomic) IBOutlet UILabel *recommendedLabel;
@property (weak, nonatomic) IBOutlet UILabel *resistanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *wireLengthLabel;
@property (weak, nonatomic) IBOutlet UILabel *electricityLabel;
@property (weak, nonatomic) IBOutlet UILabel *poweDensityLabel;
@property (weak, nonatomic) IBOutlet UILabel *coilLengthLabel;
@property (weak, nonatomic) IBOutlet UILabel *temperatureLabel;


@end