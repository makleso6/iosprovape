//
//  SCWiresInCoilCell.h
//  ProVape
//
//  Created by Maxim Kolesnik on 13.11.15.
//  Copyright © 2015 Sugar And Candy LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SCWiresInCoilCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UISwitch *twistedSwitch;
@property (weak, nonatomic) IBOutlet UILabel *twistedLabel;

@end
