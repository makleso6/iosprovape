//
//  SCBatteryCell.m
//  ProVape
//
//  Created by Maxim Kolesnik on 05.11.15.
//  Copyright © 2015 Sugar And Candy LLC. All rights reserved.
//

#import "SCBatteryCell.h"

@implementation SCBatteryCell

- (void)awakeFromNib {
    self.voltageSlider.minimumTrackTintColor = [UIColor colorWithRed:151.0f / 255.0f green:151.0f / 255.0f blue:151.0f / 255.0f alpha:1];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
