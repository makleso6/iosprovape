//
//  SCBatteryCell.h
//  ProVape
//
//  Created by Maxim Kolesnik on 05.11.15.
//  Copyright © 2015 Sugar And Candy LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SCBatteryCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UISlider *voltageSlider;

@property (weak, nonatomic) IBOutlet UILabel *totalOutputLabel;

@property (weak, nonatomic) IBOutlet UISwitch *correctionSwitch;
@end
