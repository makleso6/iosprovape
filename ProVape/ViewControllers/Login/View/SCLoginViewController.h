//
//  SCLoginSCLoginViewController.h
//  Pro Vape
//
//  Created by Sugar and Candy on 18/05/2016.
//  Copyright © 2016 Sugar and Candy. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SCLoginViewInput.h"

@protocol SCLoginViewOutput;

@interface SCLoginViewController : UIViewController <SCLoginViewInput>

@property (nonatomic, strong) id<SCLoginViewOutput> output;

@end
