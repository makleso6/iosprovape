//
//  SCLoginSCLoginViewInput.h
//  Pro Vape
//
//  Created by Sugar and Candy on 18/05/2016.
//  Copyright © 2016 Sugar and Candy. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SCLoginViewInput <NSObject>

/**
 @author Sugar and Candy

 Метод настраивает начальный стейт view
 */
- (void)setupInitialState;

@end
