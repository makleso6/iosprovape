//
//  SCLoginSCLoginInteractor.h
//  Pro Vape
//
//  Created by Sugar and Candy on 18/05/2016.
//  Copyright © 2016 Sugar and Candy. All rights reserved.
//

#import "SCLoginInteractorInput.h"

@protocol SCLoginInteractorOutput;

@interface SCLoginInteractor : NSObject <SCLoginInteractorInput>

@property (nonatomic, weak) id<SCLoginInteractorOutput> output;

@end
