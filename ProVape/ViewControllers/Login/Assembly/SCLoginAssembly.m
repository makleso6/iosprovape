//
//  SCLoginSCLoginAssembly.m
//  Pro Vape
//
//  Created by Sugar and Candy on 18/05/2016.
//  Copyright © 2016 Sugar and Candy. All rights reserved.
//

#import "SCLoginAssembly.h"

#import "SCLoginViewController.h"
#import "SCLoginInteractor.h"
#import "SCLoginPresenter.h"
#import "SCLoginRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation SCLoginAssembly

- (SCLoginViewController *)viewLoginModule {
    return [TyphoonDefinition withClass:[SCLoginViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterLoginModule]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterLoginModule]];
                          }];
}

- (SCLoginInteractor *)interactorLoginModule {
    return [TyphoonDefinition withClass:[SCLoginInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterLoginModule]];
                          }];
}

- (SCLoginPresenter *)presenterLoginModule {
    return [TyphoonDefinition withClass:[SCLoginPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewLoginModule]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorLoginModule]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerLoginModule]];
                          }];
}

- (SCLoginRouter *)routerLoginModule {
    return [TyphoonDefinition withClass:[SCLoginRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewLoginModule]];
                          }];
}

@end
