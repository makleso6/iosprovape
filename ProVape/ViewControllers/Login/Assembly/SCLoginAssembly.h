//
//  SCLoginSCLoginAssembly.h
//  Pro Vape
//
//  Created by Sugar and Candy on 18/05/2016.
//  Copyright © 2016 Sugar and Candy. All rights reserved.
//

#import <Typhoon/Typhoon.h>
#import <RamblerTyphoonUtils/AssemblyCollector.h>

/**
 @author Sugar and Candy

 Login module
 */
@interface SCLoginAssembly : TyphoonAssembly <RamblerInitialAssembly>

@end
