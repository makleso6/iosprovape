//
//  SCLoginSCLoginPresenter.h
//  Pro Vape
//
//  Created by Sugar and Candy on 18/05/2016.
//  Copyright © 2016 Sugar and Candy. All rights reserved.
//

#import "SCLoginViewOutput.h"
#import "SCLoginInteractorOutput.h"

@protocol SCLoginViewInput;
@protocol SCLoginInteractorInput;
@protocol SCLoginRouterInput;

@interface SCLoginPresenter : NSObject <SCLoginViewOutput, SCLoginInteractorOutput>

@property (nonatomic, weak) id<SCLoginViewInput> view;
@property (nonatomic, strong) id<SCLoginInteractorInput> interactor;
@property (nonatomic, strong) id<SCLoginRouterInput> router;

@end
