//
//  SCLoginSCLoginViewOutput.h
//  Pro Vape
//
//  Created by Sugar and Candy on 18/05/2016.
//  Copyright © 2016 Sugar and Candy. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SCLoginViewOutput <NSObject>

/**
 @author Sugar and Candy

 Метод сообщает презентеру о том, что view готова к работе
 */
- (void)didTriggerViewReadyEvent;

@end
