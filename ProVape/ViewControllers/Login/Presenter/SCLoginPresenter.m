//
//  SCLoginSCLoginPresenter.m
//  Pro Vape
//
//  Created by Sugar and Candy on 18/05/2016.
//  Copyright © 2016 Sugar and Candy. All rights reserved.
//

#import "SCLoginPresenter.h"

#import "SCLoginViewInput.h"
#import "SCLoginInteractorInput.h"
#import "SCLoginRouterInput.h"

@implementation SCLoginPresenter

#pragma mark - Методы SCLoginModuleInput

- (void)configureModule {
    // Стартовая конфигурация модуля, не привязанная к состоянию view
}

#pragma mark - Методы SCLoginViewOutput

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialState];
}

#pragma mark - Методы SCLoginInteractorOutput

@end
