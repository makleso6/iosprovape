//
//  SCLoginSCLoginRouter.h
//  Pro Vape
//
//  Created by Sugar and Candy on 18/05/2016.
//  Copyright © 2016 Sugar and Candy. All rights reserved.
//

#import "SCLoginRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface SCLoginRouter : NSObject <SCLoginRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
