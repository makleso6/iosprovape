//
//  SCLoginViewController.m
//  ProVape
//
//  Created by Maxim Kolesnik on 30.10.15.
//  Copyright © 2015 Sugar And Candy LLC. All rights reserved.
//

#import "SCLoginViewController.h"

typedef NS_OPTIONS(NSUInteger, SCLoginViewControllerNextView) {
    SCLoginViewControllerNextViewLogin = 1 << 0,
    SCLoginViewControllerNextViewSingup = 1 << 1,
    SCLoginViewControllerNextViewForgot = 1 << 2,
};

@interface SCLoginViewController () <SwipeViewDataSource, SwipeViewDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet SWFrameButton *loginButton;
@property (weak, nonatomic) IBOutlet SWFrameButton *singupButton;
@property (weak, nonatomic) IBOutlet SWFrameButton *forgotButton;

/************* SwipeView Views *************/

@property (strong, nonatomic) IBOutlet UIView *buttonsView;
@property (strong, nonatomic) IBOutlet UIView *authView;
@property (strong, nonatomic) IBOutlet UIView *singupView;
@property (strong, nonatomic) IBOutlet UIView *forgotView;

@property (weak, nonatomic) IBOutlet SwipeView *swipeView;

@property (nonatomic, assign) SCLoginViewControllerNextView nextViewType;

/************* NSLayoutConstraint Animation *************/

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *yCenterConstrait;

@property (strong, nonatomic) IBOutletCollection(NSLayoutConstraint) NSArray *animationLayoutConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *keyboardLayoutConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoLeftLayoutConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoRightLayoutConstraint;

@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
@property (weak, nonatomic) IBOutlet UILabel *makePerfectLabel;

/************* SwipeView TextFields *************/
@property (weak, nonatomic) IBOutlet SCPlaceholderTextField *usernameTextField;
@property (weak, nonatomic) IBOutlet SCPlaceholderTextField *passwordTextField;

@property (weak, nonatomic) IBOutlet SCPlaceholderTextField *singupUsernameTextField;
@property (weak, nonatomic) IBOutlet SCPlaceholderTextField *singupEmailTextField;
@property (weak, nonatomic) IBOutlet SCPlaceholderTextField *singupPasswordTextField;

@property (weak, nonatomic) IBOutlet SCPlaceholderTextField *fogotPasswordTextField;


@property (nonatomic, assign) BOOL viewDidLoaded;

@end

@implementation SCLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureButtons];
    
    self.swipeView.delegate = self;
    
    self.swipeView.dataSource = self;
    
    self.swipeView.wrapEnabled = NO;
    
    self.swipeView.scrollEnabled = NO;
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardHide:) name:UIKeyboardWillHideNotification object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardShow:) name:UIKeyboardWillShowNotification object:nil];
    
    
    for (NSLayoutConstraint *constr in self.animationLayoutConstraint) {
        constr.active = NO;
    }
    
    self.yCenterConstrait.active = YES;
    
    [self.view layoutIfNeeded];
}

-(void)configureButtons {
    
    [self.loginButton addTarget:self action:@selector(buttonTouch:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.singupButton addTarget:self action:@selector(buttonTouch:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.forgotButton addTarget:self action:@selector(buttonTouch:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    //self.contentView.alpha = 0;
    
    if (!self.viewDidLoaded) {
        for (NSLayoutConstraint *constr in self.animationLayoutConstraint) {
            constr.active = NO;
        }
        
        self.yCenterConstrait.active = YES;
        
        self.swipeView.alpha = 0;
        
        [self.view layoutIfNeeded];
    }
    
    
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    //self.xCenterConstrait.active = NO;
    //[NSThread sleepForTimeInterval:0.5f];
    
    if (!self.viewDidLoaded) {
        self.yCenterConstrait.active = NO;
        for (NSLayoutConstraint *constr in self.animationLayoutConstraint) {
            constr.active = YES;
        }
        
        
        [UIView animateWithDuration:1.3 animations:^{
            [self.view layoutIfNeeded];
            
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:1.3 animations:^{
                
                self.swipeView.alpha = 1;
                //self.forgotPasswordButton.alpha = 1;
                
            }];
        }];
        
        self.viewDidLoaded = YES;
    }
    
    
    
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
}

-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
}



-(void)keyboardShow:(NSNotification *)notification {
    
    
    self.keyboardLayoutConstraint.constant = 226;
    if ((IS_IPHONE4)) {
        self.logoLeftLayoutConstraint.constant = 100;
        
        self.logoRightLayoutConstraint.constant = 100;
        
    }
    
    
    [UIView animateWithDuration:0.3 animations:^{
        
        [self.view layoutIfNeeded];
        self.makePerfectLabel.alpha = 0;
        
        if (IS_IPHONE4) {
            
            self.logoImageView.alpha = 0;
        }
        
    }];
    
}

-(void)keyboardHide:(NSNotification *)notification {
    
    
    self.keyboardLayoutConstraint.constant = 20;
    
    self.logoLeftLayoutConstraint.constant = 40;
    
    self.logoRightLayoutConstraint.constant = 40;
    
    [UIView animateWithDuration:0.25 animations:^{
        self.makePerfectLabel.alpha = 1;
        
        [self.view layoutIfNeeded];
        if (IS_IPHONE4) {
            self.logoImageView.alpha = 1;
            
        }
    }];
}

-(void)buttonTouch:(UIButton *)button {
    [self.swipeView scrollToItemAtIndex:1 duration:0.4];
    
    self.swipeView.scrollEnabled = YES;
    
    if ([button isEqual:self.loginButton]) {
        self.nextViewType = SCLoginViewControllerNextViewLogin;
    }
    
    if ([button isEqual:self.singupButton]) {
        self.nextViewType = SCLoginViewControllerNextViewSingup;
    }
    
    if ([button isEqual:self.forgotButton]) {
        self.nextViewType = SCLoginViewControllerNextViewForgot;
    }
}



- (IBAction)authAction:(UIButton *)sender {
    
    [KVNProgress showWithStatus:NSLocalizedString(@"LOGGIN_IN", nil) onView:self.view];
    [self.view endEditing:YES];
    
    [PFUser logInWithUsernameInBackground:self.usernameTextField.text password:self.passwordTextField.text block:^(PFUser * _Nullable user, NSError * _Nullable error) {
        
        if (user) {
            [KVNProgress dismiss];
            [self presentProVapeViewController];
            [self saveCurrentUserToCoreData:[PFUser currentUser]];

            
        } else {
            
            //[KVNProgress dismiss];
            //NSLog(@"%@",error.localizedDescription);
            
            NSLog(@"%ld",error.code);
            switch (error.code) {
                case 101:
                    [KVNProgress showErrorWithStatus:NSLocalizedString(@"LOGIN_PASSWORD_INCORRECT", nil) onView:self.view];
                    //login or password inocorrect
                    break;
                case 100:
                    //no inet
                    [KVNProgress showErrorWithStatus:NSLocalizedString(@"LOST_CONNECTION", nil) onView:self.view];
                    
                    break;
                    
                default:
                    [KVNProgress showErrorWithStatus:NSLocalizedString(@"ERROR", nil) onView:self.view];
                    
                    break;
            }
        }
        
    }];
}

- (IBAction)singupAction:(UIButton *)sender {
    
    [KVNProgress showWithStatus:NSLocalizedString(@"SINGING_UP", nil) onView:self.view];
    [self.view endEditing:YES];
    
    PFUser* user = [PFUser user]; // not the current user!
    
    user.username = self.singupUsernameTextField.text;
    
    user.email = self.singupEmailTextField.text;
    
    user.password = self.singupPasswordTextField.text;
    
    [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
        if (error) {
            NSLog(@"Error saving user: %@", error);
            
            switch (error.code) {
                case 125:
                    //invalid email address
                    [KVNProgress showErrorWithStatus:NSLocalizedString(@"INVALID_EMAIL", nil) onView:self.view];
                    
                    break;
                case 202:
                    [KVNProgress showErrorWithStatus:NSLocalizedString(@"USERNAME_TAKEN", nil) onView:self.view];
                    
                    //username makleso6 already taken
                    break;
                case 203:
                    [KVNProgress showErrorWithStatus:NSLocalizedString(@"EMAIL_TAKEN", nil) onView:self.view];
                    
                    //email already taken
                    break;
                case 200:
                    [KVNProgress showErrorWithStatus:NSLocalizedString(@"MISSING_USERNAME", nil) onView:self.view];
                    
                    //email already taken
                    break;
                    
                default:
                    [KVNProgress showErrorWithStatus:NSLocalizedString(@"ERROR", nil) onView:self.view];
                    
                    break;
            }
        } else {
            NSLog(@"User %@ saved! You are logged in as %@", user, [PFUser currentUser]);
            
            NSLog(@"%@",[PFUser currentUser].sessionToken);
            [KVNProgress dismiss];
            
            [self saveCurrentUserToCoreData:[PFUser currentUser]];
            
            [self presentProVapeViewController];
        }
    }];
}

-(void)saveCurrentUserToCoreData:(PFUser*)currentUser {
    
    User *findUser = [User MR_findFirstByAttribute:BaseAttributes.identifier withValue:currentUser.objectId];
    
    if (!findUser) {
        User *user = [User MR_createEntity];
        
        user.identifier = currentUser.objectId;
        
        [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError *error) {
            //[[SCSynchronizationManager standartManager] synchronizeWithServer:^(BOOL success, NSError *error) {
//                [[SCSynchronizationManager standartManager] synchronizeWithLocalStorage:nil];
//                
//            }];

            
        }];
        

    }
    
    
    
}


- (IBAction)resetPasswordAction:(UIButton *)sender {
    
    //[self makeAlertWithTitle:@"title" message:@"message"];
    
    [KVNProgress showWithStatus:@"Wait" onView:self.view];
    [self.view endEditing:YES];
    
    [PFUser requestPasswordResetForEmailInBackground:self.fogotPasswordTextField.text block:^(BOOL succeeded, NSError * _Nullable error) {
        if (succeeded) {
            NSLog(@"УСПЕХ");
            
            [KVNProgress showSuccessWithStatus:NSLocalizedString(@"INSTRUCTIONS_SEND", nil)];

        } else {
            
            NSLog(@"%ld",error.code);
            
            switch (error.code) {
                case 205:
                    NSLog(@"EMAIL DOES NOT EXIST");
                    
                    [KVNProgress showErrorWithStatus:NSLocalizedString(@"EMAIL_DOES_NOT_EXIST", nil) onView:self.view];
                    
                    break;
                    
                case 125:
                    NSLog(@"EMAIL IS NOT VALID");
                    [KVNProgress showErrorWithStatus:NSLocalizedString(@"EMAIL_IS_INVALID", nil) onView:self.view];
                    
                    break;
                default:
                    [KVNProgress showErrorWithStatus:NSLocalizedString(@"ERROR", nil) onView:self.view];
                                        
                    NSLog(@"LATER");
                    
                    break;
            }
        }
    }];
}

-(void)presentProVapeViewController {
    UIStoryboard *board = [UIStoryboard pv_getProVapeStoryboard];
    
    UIViewController *rootViewController = [board instantiateInitialViewController];
    
    rootViewController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    
    [self presentViewController:rootViewController animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfItemsInSwipeView:(SwipeView *)swipeView
{
    //return the total number of items in the carousel
    return 2;
}

- (UIView *)swipeView:(SwipeView *)swipeView viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    switch (index) {
        case 0:
            return self.buttonsView;
            break;
        case 1:{
            
            switch (self.nextViewType) {
                case SCLoginViewControllerNextViewLogin:
                    return self.authView;
                    
                    break;
                case SCLoginViewControllerNextViewSingup:
                    return self.singupView;
                    
                    break;
                case SCLoginViewControllerNextViewForgot:
                    return self.forgotView;
                    
                    break;
                    
                default:
                    break;
            }
            
            break;
        }
        default:
            break;
    }
    
    return [UIView new];
}



- (CGSize)swipeViewItemSize:(SwipeView *)swipeView
{
    return self.swipeView.bounds.size;
}



- (void)swipeViewDidEndDecelerating:(SwipeView *)swipeView{
    if (swipeView.currentItemIndex == 0) {
        self.swipeView.scrollEnabled = NO;
        
    }
}

#pragma mark - Touches

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [[self view] endEditing:YES];
}

#pragma mark - UITextFieldDelegate

-(BOOL)textFieldShouldReturn:(UITextField *)theTextField {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    //    if (theTextField == self.emailTextField) {
    //        [theTextField resignFirstResponder];
    //
    //        [self.passwordTextField becomeFirstResponder];
    //    }
    //    if (theTextField == self.forgotPasswordTextField) {
    //        [theTextField resignFirstResponder];
    //
    //        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardHide:) name:UIKeyboardWillHideNotification object:nil];
    //
    //        //[self.view endEditing:YES];
    //        [self restoreButtonAction:nil];
    //    }
    //    if (theTextField == self.passwordTextField) {
    //        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardHide:) name:UIKeyboardWillHideNotification object:nil];
    //
    //        [self.passwordTextField resignFirstResponder];
    //        [self connect];
    //
    //
    //    }
    /*
     
     @property (weak, nonatomic) IBOutlet SCPlaceholderTextField *usernameTextField;
     @property (weak, nonatomic) IBOutlet SCPlaceholderTextField *passwordTextField;
     
     @property (weak, nonatomic) IBOutlet SCPlaceholderTextField *singupUsernameTextField;
     @property (weak, nonatomic) IBOutlet SCPlaceholderTextField *singupEmailTextField;
     @property (weak, nonatomic) IBOutlet SCPlaceholderTextField *singupPasswordTextField;
     
     @property (weak, nonatomic) IBOutlet SCPlaceholderTextField *fogotPasswordTextField;*/
    if (theTextField == self.usernameTextField) {
        [self.usernameTextField resignFirstResponder];
        
        [self.passwordTextField becomeFirstResponder];
    }
    if (theTextField == self.passwordTextField) {
        [self.passwordTextField resignFirstResponder];
        [self authAction:nil];
    }
    
    if (theTextField == self.singupUsernameTextField) {
        [self.singupUsernameTextField resignFirstResponder];
        
        [self.singupEmailTextField becomeFirstResponder];
    }
    if (theTextField == self.singupEmailTextField) {
        [self.singupEmailTextField resignFirstResponder];
        
        [self.singupPasswordTextField becomeFirstResponder];
    }
    if (theTextField == self.singupPasswordTextField) {
        [self.singupPasswordTextField resignFirstResponder];
        [self singupAction:nil];
    }
    
    if (theTextField == self.singupPasswordTextField) {
        [self.fogotPasswordTextField resignFirstResponder];
        
        [self resetPasswordAction:nil];
    }
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardHide:) name:UIKeyboardWillHideNotification object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardShow:) name:UIKeyboardWillShowNotification object:nil];
    return YES;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
