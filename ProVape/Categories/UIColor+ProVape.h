//
//  UIColor+ProVape.h
//  ProVape
//
//  Created by Maxim Kolesnik on 30.10.15.
//  Copyright © 2015 Sugar And Candy LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (ProVape)

+(UIColor *)pv_whiteColor;

+(UIColor *)pv_lightBlackColor;

+(UIColor *)pv_navigationBarColor;

+(UIColor *)pv_darkBlackColor;

+(UIColor *)pv_darkGrayColor;

+(UIColor *)pv_grayColor;

@end
