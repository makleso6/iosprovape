//
//  UIColor+ProVape.m
//  ProVape
//
//  Created by Maxim Kolesnik on 30.10.15.
//  Copyright © 2015 Sugar And Candy LLC. All rights reserved.
//

#import "UIColor+ProVape.h"

@implementation UIColor (ProVape)

+(UIColor *)pv_whiteColor {
    return [UIColor colorWithRed:242.0f / 255.0f green:242.0f / 255.0f blue:242.0f / 255.0f alpha:1.0f];
}

+(UIColor *)pv_lightBlackColor{
    
    return [UIColor colorWithRed:36.0f / 255.0f green:36.0f / 255.0f blue:36.0f / 255.0f alpha:1.0f];

}

+(UIColor *)pv_navigationBarColor {

    return [UIColor colorWithRed:43.0f / 255.0f green:43.0f / 255.0f blue:43.0f / 255.0f alpha:1.0f];

}

+(UIColor *)pv_darkBlackColor{
    
    return [UIColor colorWithRed:28.0f / 255.0f green:28.0f / 255.0f blue:28.0f / 255.0f alpha:1.0f];
    
}

+(UIColor *)pv_darkGrayColor{

    return [UIColor colorWithRed:90.0f / 255.0f green:90.0f / 255.0f blue:90.0f / 255.0f alpha:1.0f];

}

+(UIColor *)pv_grayColor {
    return [UIColor colorWithRed:116.0f / 255.0f green:116.0f / 255.0f blue:116.0f / 255.0f alpha:1.0f];

}

@end
