//
//  UIStoryboard+ProVape.m
//  ProVape
//
//  Created by Maxim Kolesnik on 31.10.15.
//  Copyright © 2015 Sugar And Candy LLC. All rights reserved.
//

#import "UIStoryboard+ProVape.h"

@implementation UIStoryboard (ProVape)

+(UIStoryboard *)pv_getCoilStoryboard{

    UIStoryboard *board = [UIStoryboard storyboardWithName:@"Coil" bundle:nil];
    return board;
}

+(UIStoryboard *)pv_getFlavorMakeStoryboard {
	
    UIStoryboard *board = [UIStoryboard storyboardWithName:@"CoilMake" bundle:nil];
    return board;
}

+(UIStoryboard *)pv_getFluidMakeStoryboard {
	
    UIStoryboard *board = [UIStoryboard storyboardWithName:@"CoilMake" bundle:nil];
    return board;
}

+(UIStoryboard *)pv_getMainStoryboard {
	
    UIStoryboard *board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    return board;
}

+(UIStoryboard *)pv_getProVapeStoryboard {
	
    UIStoryboard *board = [UIStoryboard storyboardWithName:@"ProVape" bundle:nil];
    return board;
}

@end
