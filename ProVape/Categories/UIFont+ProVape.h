//
//  UIFont+ProVape.h
//  ProVape
//
//  Created by Maxim Kolesnik on 12.11.15.
//  Copyright © 2015 Sugar And Candy LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (ProVape)

+(UIFont *)pv_AvenurNextDefault;

+(UIFont *)pv_AvenurNextSmall;

+(UIFont *)pv_AvenurNextBig;


@end
