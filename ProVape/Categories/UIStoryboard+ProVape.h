//
//  UIStoryboard+ProVape.h
//  ProVape
//
//  Created by Maxim Kolesnik on 31.10.15.
//  Copyright © 2015 Sugar And Candy LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIStoryboard (ProVape)

+(UIStoryboard *)pv_getCoilStoryboard;

+(UIStoryboard *)pv_getFlavorMakeStoryboard;

+(UIStoryboard *)pv_getFluidMakeStoryboard;

+(UIStoryboard *)pv_getMainStoryboard;

+(UIStoryboard *)pv_getProVapeStoryboard;



@end
