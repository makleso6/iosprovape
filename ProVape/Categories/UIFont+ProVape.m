//
//  UIFont+ProVape.m
//  ProVape
//
//  Created by Maxim Kolesnik on 12.11.15.
//  Copyright © 2015 Sugar And Candy LLC. All rights reserved.
//

#import "UIFont+ProVape.h"

@implementation UIFont (ProVape)


+(UIFont *)pv_AvenurNextDefault {
	
    return [UIFont fontWithName:AVENIR_NEXT_REGULAR size:MID_TEXT_SIZE];
}

+(UIFont *)pv_AvenurNextSmall {
    return [UIFont fontWithName:AVENIR_NEXT_REGULAR size:SMALL_TEXT_SIZE];

}

+(UIFont *)pv_AvenurNextBig {
    return [UIFont fontWithName:AVENIR_NEXT_REGULAR size:BIG_TEXT_SIZE];

}

@end
